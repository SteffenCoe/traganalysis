# -*- coding: utf-8 -*-

def run():
    import matplotlib
    import os
    cwd = os.getcwd()
    
    matplotlib.pyplot.rcdefaults() #use this to change matplotlib-style-values to default
    matplotlib.pyplot.style.use(cwd+"\\config\\"+"matplotlib_style.mplstyle")
    matplotlib.pyplot.close("all")
