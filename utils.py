# -*- coding: utf-8 -*-
"""
Useful functions for plotting, fits and more.

Based on the Praktikum.py file received during my Grundpraktikum I in Physics 
during my Bachelor's degree in 2017. Complemented by many new custom 
functions, that aim at creating plots with a consistent style and helping 
the user to calculate fits and perform other statistical operations.

This file includes the functions relevant for the trace gas analysis software framework.

Original header:
"
Created on Wed Mar  5 18:03:48 2014
Useful tools for Grundpraktikum Physik, based on MAPLE sheet Praktikum.mws
@author: henning

# A widely useful file with methods for reading in data, 
# calculating statistical values, and create histograms and linear fits.
"
"""

import os
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt

t_rotation = "horizontal" #used as tick rotation
l_scilim = -5 #10^(l_scilim) used as left scilimit
r_scilim = 5 #10^(r_scilim) used as right scilimit

pad_inches = 0.05

def read_config_file(filename, folder="config/"):
    """
    Reads-in a configuration file using the configparser object.

    Parameters
    ----------
    filename : string
        name of the configuration file to read information from
    folder : string, optional
        path of configuration file. The default is "config/".

    Returns
    -------
    config : ConfigParser object
        ConfigParser object of concerning configuration file.
    """
    
    import configparser

    config = configparser.ConfigParser()
    config.read(folder+filename)
    return config

def get_date_from_xlsfloat(date_as_float):
    """
    Returns a datetime object based on an Excel date floating point number.
    
    Excel counts the numbers of days since January 1, 1900. So, for example, 
    January 1, 2021, has the value 44197. With decimal digits, one can also 
    sepcify the time, such as 44197.5 for 12:00pm (noon) on January 1, 2021.

    Parameters
    ----------
    date_as_float : float
        An Excel date floating point number that is to be converted to a datetime object.

    Returns
    -------
    The datetime object with the correct date.

    """
    
    import datetime
    
    temp = datetime.datetime(1900, 1, 1)
    delta = datetime.timedelta(days=date_as_float-2)
    return temp+delta

def purge(dir, pattern, safe_mode=True):
    """
    Purges (cleanes, i.e. removes) a folder dir with files matching the pattern.

    Parameters
    ----------
    dir : string
        The path to the folder that needs to be purged of certain files.
    pattern : string
        The pattern the files need to match to be deleted.
    safe_mode : bool, optional
        Switch whether the function prompts user for confirmation before 
        deleting any files. Prompt can be replied to with 'y' (for yes, delete 
        this file), 'n' (for no, do not delete this file), or 'all' (to delete 
        all remaining files in the folder that match the pattern).
        The default is True.
    """
    import re
    
    if os.path.exists(dir):
        for f in os.listdir(dir):
            if re.search(pattern, f):
                if safe_mode:
                    inp = input("Want to delete file '%s'? (y/n) "%f)
                    if inp=="n":
                        continue
                    elif inp in ["y","all"]:
                        os.remove(os.path.join(dir, f))
                        # print("deleted", f)
                        if inp=="all":
                            safe_mode = False
                    else:
                        print("purge(): could not deal with your user input, did not delete the file.")
                else:
                    os.remove(os.path.join(dir, f))
                    # print("deleted", f)
    else:
        print("purge(): path '%s' does not exist, so could not purge anything."%dir)

def nearest(items, pivot):
    """
    Returns the element in items that is closest to pivot.

    Parameters
    ----------
    items : iterable
        A list or an array containing elements like numbers, vectors, or datetimes.
    pivot : float, vector, or datetimes
        The number/object the closest element in items should be found to.

    Returns
    -------
    The element in items that is closest to pivot.
    """
    
    return min(items, key=lambda x: abs(x - pivot))

def nth_repl_all(s, sub, repl, nth):
    """
    Replaces nth occurence of a substring sub in a string s with repl.
    """
    
    find = s.find(sub)
    # loop util we find no match
    i = 1
    while find != -1:
        # if i  is equal to nth we found nth matches so replace
        if i == nth:
            s = s[:find]+repl+s[find + len(sub):]
            i = 0
        # find + len(sub) + 1 means we start after the last match
        find = s.find(sub, find + len(sub) + 1)
        i += 1
    return s

def select_random_elements_from_list(l, n, sort=True):
    """
    Selects n random elemnts from the given list l.
    """
    
    import random
    
    if type(l) is not list:
        l = list(l)
    
    sample = random.sample(l, n)
    
    if sort:
        sample.sort()
    
    return sample

def lineare_regression(x,y,ey):
    '''

    Lineare Regression.

    Parameters
    ----------
    x : array_like
        x-Werte der Datenpunkte
    y : array_like
        y-Werte der Datenpunkte
    ey : array_like
        Fehler auf die y-Werte der Datenpunkte

    Diese Funktion benoetigt als Argumente drei Listen:
    x-Werte, y-Werte sowie eine mit den Fehlern der y-Werte.
    Sie fittet eine Gerade an die Werte und gibt die
    Steigung a und y-Achsenverschiebung b mit Fehlern
    sowie das chi^2 und die Korrelation von a und b
    als Liste aus in der Reihenfolge
    [a, ea, b, eb, chiq, cov].
    '''
    
    if len(ey)==1:
        ey = np.ones(len(y))*ey[0]
    
    x = np.asarray(x)
    y = np.asarray(y)
    ey = np.asarray(ey)

    s   = sum(1./ey**2)
    sx  = sum(x/ey**2)
    sy  = sum(y/ey**2)
    sxx = sum(x**2/ey**2)
    sxy = sum(x*y/ey**2)
    delta = s*sxx-sx*sx
    #print s,sx,sy,sxx,sxy,delta
    b   = (sxx*sy-sx*sxy)/delta
    a   = (s*sxy-sx*sy)/delta
    eb  = np.sqrt(sxx/delta)
    ea  = np.sqrt(s/delta)
    cov = -sx/delta
    corr = cov/(ea*eb)
    chiq  = sum(((y-(a*x+b))/ey)**2)

    return(a,ea,b,eb,chiq,corr)

def basic_stats(series):
    """
    Calculates and returns basic statistical quantitites of a pandas Series object.

    Parameters
    ----------
    series : pandas.Series
        The series the statistical quantities should be calcucated for.

    Returns
    -------
    out : pandas.Series
        The series that contains the results. The index contains the names of 
        all derived quantities.
    """
    
    out = pd.Series(name=series.name)
    out.loc["mean"] = np.nanmean(series)
    out.loc["std"] = np.nanstd(series, ddof=1)
    out.loc["mean_std"] = out.loc["std"]/np.sqrt(len(series.index))
    out.loc["min"] = np.min(series)
    out.loc["max"] = np.max(series)
    out.loc["05perc"] = np.nanpercentile(series, 5)
    out.loc["25perc"] = np.nanpercentile(series, 25)
    out.loc["median"] = np.nanmedian(series)
    out.loc["75perc"] = np.nanpercentile(series, 75)
    out.loc["95perc"] = np.nanpercentile(series, 95)
    return out

def swap_columns(df, c1, c2):
    """
    Swaps the content of two columns in a dataframe.

    Parameters
    ----------
    df : pandas.DataFrame
        The dataframe in which two columns' contents should be swapped.
    c1 : string
        Column name 1.
    c2 : string
        Column name 2..
    """
    
    df['temp'] = df[c1]
    df[c1] = df[c2]
    df[c2] = df['temp']
    df.drop(columns=['temp'], inplace=True)

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    """
    Truncates a matplotlib colormap.
    
    doc and credits: https://stackoverflow.com/questions/18926031/how-to-extract-a-subset-of-a-colormap-as-a-new-colormap-in-matplotlib

    Parameters
    ----------
    cmap : matplotlib.colors.Colormap
        A colormap that is supposed to be truncated.
    minval : float, optional
        Left/lower boundary of the truncation. The default is 0.0.
    maxval : float, optional
        Right/upper boundary of the truncation. The default is 1.0.
    n : int, optional
        Desired number of colors. The default is 100.

    Returns
    -------
    new_cmap : matplotlib.colors.Colormap
        The truncated colormap.
    """
    
    import matplotlib.colors as colors
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap

def ordinal(n):
    """
    Returns the ordinal as a string for a given number (integer).
    
    Example: ordinal(5) == "5th"

    Parameters
    ----------
    n : int
        Number that the ordinal should be returned for. 
        For non-integer inputs, the number is rounded to an integer first.

    Returns
    -------
    The ordinal string.
    """
    
    if type(n)!=int:
        n = int(n)
    return "%d%s" % (n,"tsnrhtdd"[(np.floor(n/10)%10!=1)*(n%10<4)*n%10::4])

def add_common_ytitle(ytitle, figpos=111, fig=None):
    """
    Add a common ytitle to an existing figure of several subplots.
    
    Useful if all subplots in one column need same ytitle (this way, 
    repeating the same ytitle for each subplot in the column can be 
    avoided.
    
    Parameters
    ----------
    ytitle : string
        The ytitle to be set.
    figpos : int or tuple
        Position of the (virtual) figure that is created to place the yitle.
    fig : plt.figure() object
        The figure is taken as plt.gcf() if not provided otherwise.
    """
    
    if fig is None:
        fig = plt.gcf()
    fig.add_subplot(figpos, frame_on=False)
    plt.tick_params(labelcolor="none", bottom=False, left=False)
    plt.grid(0)
    plt.ylabel(ytitle)

def plot(x, y, ey=[], ex=[], frame=[], kind="scatter", marker_option=".", 
         ls="-", lw=1, label="", color="royalblue", zorder=1, alpha=1., 
         output_folder="", filename=""):
    """
    Erstellt einen Plot (plot, scatter oder errorbar).
    
    Parameters
    ----------
    x : array-like
        x-Werte
    y : array-like
        y-Werte
    ey : array_like
        Fehler auf die y-Werte
    ex : array_like
        Fehler auf die x-Werte
    kind : string
        Die Art des plots
        Möglich sind "plot" (default), "scatter" und "errorbar".
    marker_option : string
        Definiert die Option marker bei Plottyp "plot" oder "scatter" sowie 
        die Option fmt bei Plottyp "errorbar".
    ls : string
        linestyle
    lw : float
        linewidth
    zorder : int
        Die "Ebene" der zu plottenden Daten
        
    return frame
    """
    #error arrays
    if len(ex)==1:
        ex = np.ones(len(x))*ex[0]
    elif ex==[]:
        ex = np.zeros(len(x))
    if len(ey)==1:
        ey = np.ones(len(y))*ey[0]
    
    #plotting
    fig, plot = plt.subplots(1,1) if frame == [] else frame
    if kind=="plot":
        plot.plot(x, y, color=color, marker=marker_option, ls=ls, lw=lw, label=label, zorder=zorder, alpha=alpha)
    elif kind=="scatter":
        plot.scatter(x, y, color=color, marker=marker_option, lw=lw, label=label, zorder=zorder, alpha=alpha)
    elif kind=="errorbar":
        plot.errorbar(x, y, ey, ex, color=color, fmt=marker_option, ls="", lw=lw, label=label, zorder=zorder, alpha=alpha)
    elif kind=="bar":
        plot.bar(x, y, color=color, label=label, zorder=zorder, alpha=alpha)
    
    #saving plot
    if filename!="":
        fig.savefig(output_folder+filename,bbox_inches='tight',pad_inches=pad_inches)
    
    return [fig,plot]

def fig_ax_setup(fig, suptitle=None, title=None, 
                 xlabel=None, ylabel=None, 
                 xlim=None, ylim=None, 
                 xticks=None, yticks=None, 
                 xtick_rotation=None, ytick_rotation=None, 
                 xscale="linear", yscale="linear", 
                 grid=True, legend_position="best", bbox_to_anchor=None, ncol=1, 
                 y_suptitle=1.05, axis_formatting=False, 
                 filename=None, dpi=None, transparent=0):
    
    """
    Defines the setup of a plot.
    
    Parameters
    ----------
    fig : matplotlib.Figure object (or tuple of length 2 ([fig,ax]))
        Figure object (or [fig,ax] tuple) to be set-up.
    suptitle : string
        (superordinate) title of the figure object
    title : string
        title of the axis object
    xlabel / ylabel : string
        labels/quanitities on x- and y-axis
    xlim / ylim : tuple
        axes' limits
    xticks / yticks : list
        axes' ticks
    xscale / yscale : string
        {"linear","log"}. Default is "linear".
    xtick_rotation / ytick_rotation : string or int
        Rotation of the x- and y-axis, respectively.
        Default is None.
    grid : bool
        True, if a grid is wanted. Else False.
    legend_position : None or int or string
        legend position defined by an integer or a string like "upper left". 
        Choose None for no legend.
        Default is "best".
    bbox_to_anchor : None or tuple
        Position of legend box anchor. 
        Use bbox_to_anchor=(1, 0.5) and legend_position="center left" to place 
        legend to the right of the figure.
    ncol : int
        Number of columns in the legend. 
        Default is 1.
    y_suptitle : float
        y-position of the suptitle in factors of the y-size of the figure.
        Default is 1.05.
    axis_formatting : bool
        Boolean whether to do axis_formatter commands or not.
    """
    
    #frame of figure
    if type(fig) is list or type(fig) is tuple:
        fig,ax = fig
    else: #new standard case
        ax = fig.get_axes()
    
    try:
        ax_list = list(ax)
    except:
        ax_list = [ax]
    n_ax = len(ax_list)
    
    #suptitle and title
    if suptitle is not None:
        fig.suptitle(suptitle,y=y_suptitle)
    if title is not None:
        if n_ax!=1:
            for ax,i in zip(ax_list,list(range(n_ax))):
                ax.set_title(title[i])
        else:
            ax_list[0].set_title(title)
    
    #axes labels, limits, ticks, scilimits, and scale
    if ylabel is not None:
        ax_list[0].set_ylabel(ylabel)
    for ax in ax_list:
        if xlabel is not None:
            ax.set_xlabel(xlabel)
        if xlim is not None:
            ax.set_xlim(xlim)
        if ylim is not None:
            ax.set_ylim(ylim)
        if xticks is not None:
            ax.set_xticks(xticks)
        if yticks is not None:
            ax.set_yticks(yticks)
        if xtick_rotation is not None:
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_rotation(xtick_rotation)
        if ytick_rotation is not None:
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_rotation(ytick_rotation)
        if axis_formatting:
            # axis_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
            ax.ticklabel_format(style="sci",axis="x",scilimits=(l_scilim,r_scilim))
            ax.ticklabel_format(style="sci",axis="y",scilimits=(l_scilim,r_scilim))
            # axis_formatter = matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ','))
            axis_formatter = matplotlib.ticker.FuncFormatter(y_fmt)
            ax.xaxis.set_major_formatter(axis_formatter)
            ax.yaxis.set_major_formatter(axis_formatter)
        if ax.get_xscale()!=xscale:
            ax.set_xscale(xscale)
        if ax.get_yscale()!=yscale:
            ax.set_yscale(yscale)
        
        #grid
        ax.grid(grid,zorder=0)
        
        #legend
        if legend_position is not None and len(ax.get_legend_handles_labels()[1])>0:
            ax.legend(loc=legend_position, bbox_to_anchor=bbox_to_anchor, ncol=ncol)
    
    #saving plot
    if filename is not None:
        save_figure(fig, filename, dpi, transparent)

def y_fmt(y, pos):
    decades = [1e9, 1e6, 1e3, 1e0, 1e-3, 1e-6, 1e-9 ]
    suffix  = ["G", "M", "k", "" , "m" , "mu", "n"  ]
    if y == 0:
        return str(0)
    for i, d in enumerate(decades):
        if np.abs(y) >=d:
            val = y/float(d)
            signf = len(str(val).split(".")[1])
            if signf == 0:
                return '{val:d} {suffix}'.format(val=int(val), suffix=suffix[i])
            else:
                if signf == 1:
                    # print(val, signf)
                    if str(val).split(".")[1] == "0":
                       return '{val:d} {suffix}'.format(val=int(round(val)), suffix=suffix[i]) 
                tx = "{"+"val:.{signf}f".format(signf = signf) +"} {suffix}"
                return tx.format(val=val, suffix=suffix[i])

                #return y
    return y

def save_figure(fig, filename="figure.png", dpi=None, transparent=0):
    """
    Saves a figure that has already been set-up before completely.
    
    Parameters
    ----------
    fig : matplotlib.Figure object (or tuple of length 2 ([fig,ax]))
        Figure object (or [fig,ax] tuple) to be saved as a file.
    TODO
    """
    
    #frame of figure
    if type(fig) is list or type(fig) is tuple:
        fig,ax = fig
    else: #standard case
        ax = fig.get_axes()
    
    #create path if it does not exist yet
    path = "/".join(filename.split("/")[:-1])
    if not os.path.exists(path):
        os.makedirs(path)
    
    if filename is not None:
        if transparent in [0,1]:
            fig.savefig(filename, dpi=dpi, bbox_inches='tight', pad_inches=pad_inches, transparent=transparent)
        else: #transparency with alpha between 0 and 1
            alpha = transparent
            fig.patch.set_facecolor('white'), fig.patch.set_alpha(alpha)
            
            try:
                ax.patch.set_facecolor('white'), ax.patch.set_alpha(alpha)
            except: #ax is in fact a list of ax objects
                for a in ax:
                    try:
                        a.patch.set_facecolor('white'), a.patch.set_alpha(alpha)
                    except: #ax is in fact a list of list of ax objects
                        for b in a:
                            b.patch.set_facecolor('white'), b.patch.set_alpha(alpha)
            
            # If we don't specify the edgecolor and facecolor for the figure when
            # saving with savefig, it will override the value we set earlier!
            fig.savefig(filename, dpi=dpi, bbox_inches='tight', pad_inches=pad_inches, facecolor=fig.get_facecolor(), edgecolor='none')

def n_sigma(exp_value, exp_std, true_value=0.):
    return abs(exp_value-true_value)/exp_std

def shiftedColorMap(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    '''
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero.

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower offset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to 
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax / (vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highest point in the colormap's range.
          Defaults to 1.0 (no upper offset). Should be between
          `midpoint` and 1.0.
    '''
    
    import matplotlib
    from mpl_toolkits.axes_grid1 import AxesGrid
    
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }

    # regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False), 
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))

    newcmap = matplotlib.colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap
