# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 12:20:08 2020

@author: steff
"""

###############################################################################
""" Imports. """

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle
from sklearn.preprocessing import StandardScaler
import sklearn.decomposition as skldec
from adjustText import adjust_text

import methods
import utils as pu

from GLOBAL import *

###############################################################################
""" Class definitions and implementations. """

class Dataset:
    """
    This is the major container class defined to work with a given 
        * DATASET OF TRACE GAS CONCENTRATION MEASUREMENTS *
    at a site and, optionally, meteorological data in the measurement period 
    at that same site.
    
    Each member function has its own documentation as to how to use it and 
    which parameters can be passed.
    
    The main variable to work with is the self.df pandas DataFrame object. 
    After reading the data that is to be studied from an Excel sheet or a 
    csv file, this self.df contains all concentration measurements, which can 
    then be easily accessed in various ways.
    
    There has to be one configuration file created for each dataset that is to 
    be studied, specifying details about timezones, the measurement site 
    ('station'), the measured gases, custom groups of these gases, 
    the measured meteorological quantities, and more (such as details about 
    conducted NMFs).
    
    A measurement site's details can be specified in a corresponding 
    configuration file, too. For more on this, see the documentation of the 
    class Station.
    
    Highlights:
        - Time series plotting, calculation of moving averages with custom averagging interval (plot(), calc_CMA())
        - Pick custom time periods or a certain set of gases to only study a subset of the existing data (pick_date_interval(), set_gaslist())
        - Automatic calculation of all basic statistical quantities of the dataset (calc_stats())
        - Calculation and visualization of correlation indices e.g. by matrices (correlation_matrix())
        - Display of windroses and directional cumulative probabiltiy functions (CPFs) (windroses(), windroses_CPF())
        - Methods for source apportionment including
            - Principal Component Analysis (PCA())
            - Non-negative matrix factorization (NMF() and related methods)
        - Automatic detection of extreme concentration events (find_extreme_conc_events(), gather_extreme_conc_events())
    """
    
    def __init__(self, configfile, datafile, meteofile=None):
        self.configfile = configfile
        self.datafile = datafile
        self.meteofile = meteofile
        
        #class variables
        self.name = ""
        self.timezone = ""
        self.startdate = pd.to_datetime("1900-01-01 00:00:00")
        self.enddate = pd.to_datetime("1900-01-01 00:00:00")
        
        self.gases = []
        self.num_gases = 0
        self.station = ""
        self.n_tot_measurements = 0
        self.gaslist = "all gases"
        self.datecut = False
        self.cleaned = False
        
        self.df = pd.DataFrame()
        self.properties = pd.DataFrame(columns=["index", "num_measurements", "color", "unit", "uncertainty", "short name", "name", "chemical formula"], index=METEO_COLUMNS)
        self.stats = pd.DataFrame(columns=self.gases)
        self.CMAdf = dict()
        self.NMFs = dict()
        self.hasNMF = False
        self.fname_core = ""
        self.folder = ""
        
        self.initialize_config_info()
        
        self.datacolumns = ["startdate_JD", "startdate", "enddate_JD", "enddate"]
        for gas in self.gases:
            self.datacolumns += [gas, "flag_"+gas]
        
        #print(self)
        print(self.short_info())
        #print(self.station)
    
    def __str__(self):
        return "dataset info:\n\tname           = {0:s}\n\tstation        = {1:s}\n\ttimezone       = {2:s}\n\tstartdate      = {3}\n\tenddate        = {4}\n\tmeasured gases = {5:s}\n\tnumber of measured gases = {6:d}\n".format(self.name, self.station.name, self.timezone, self.startdate, self.enddate, str(list(self.gases))[1:-1], self.num_gases)
    
    def short_info(self):
        return "dataset short info:\n\tstation   = {0:s}\n\tstartdate = {1}\n\tenddate   = {2}\n\tnumber of measured gases = {3:d}\n".format(self.station.name, self.startdate, self.enddate, self.num_gases)
    
    def get_fnames(self, cleaned=None):
        folder = "data/"
        add = ""
        if cleaned is not None:
            self.cleaned = cleaned
        if self.cleaned:
            add += "_cleaned"
        df_fn = folder+self.fname_core+add+".csv"
        properties_fn = folder+self.fname_core+"_properties"+add+".csv"
        dataset_fn = folder+"bin/"+self.fname_core+add+".pkl"
        
        return df_fn, properties_fn, dataset_fn
    
    def read_dataset(self, cleaned=True, fname_core="dataset"):
        """
        Reads a dataset from a csv-file and stores it in self.df.
        
        Note: A dataset can also be easily loaded using:
        with open("data/bin/dataset_cleaned.pkl", "rb") as fobj:
            data = pickle.load(fobj)

        Parameters
        ----------
        cleaned : bool
            Boolean whether to read-in cleaned or not-yet-cleaned data.
        fname_core : string, optional
            The filename's "core". Must correspond to the filename that was 
            given in save_dataset().
            The default is "dataset".
        """
        
        self.fname_core = fname_core
        df_fn, properties_fn, dataset_fn = self.get_fnames(cleaned)
        
        self.df = pd.read_csv(df_fn, delimiter=";", index_col=0, parse_dates=["startdate", "enddate", "date_full_hour"])
        self.n_tot_measurements = len(self.df.index)
        self.properties = pd.read_csv(properties_fn, delimiter=";", index_col=0)
        self.properties["color"] = self.properties["color"].apply(eval) #to make the datatype of the column color tuple
        self.properties["unit"] = self.properties["unit"].fillna("")
        self.cleaned = cleaned
        
        print("read_dataset(): Read-in {0:s} (as def. in {1:s}).\n".format(
              self.gaslist, self.configfile))
    
    def save_dataset(self, fname_core="dataset", only_gases=False, only_bin=False):
        """
        Saves the dataset's DataFrames self.df as well as self.properties 
        into csv-files to be used later. In addition, a binary file is being 
        stored in data/bin/ that contains the entire object in its current 
        state (can be read again by read_dataset()).

        Parameters
        ----------
        fname_core : string, optional
            The desired filename's "core". 
            The default is "dataset".
        only_gases : bool
            Boolean whether or not only startdate and gases columns and an 
            uncertainty dataframe are to be saved as well. These files can 
            to be used e.g. for an EPA PMF or other further studies.
        only_bin : bool
            Boolean whether or not to only save the binary pickle file and not 
            the two csv files.
        """
        
        self.fname_core = fname_core
        df_fn, properties_fn, dataset_fn = self.get_fnames()
        
        path = "/".join(dataset_fn.split("/")[:-1])
        if not os.path.exists(path):
            os.makedirs(path)
        
        if not only_bin:
            self.df.to_csv(df_fn, sep=";", float_format="%.3f")
            self.properties.to_csv(properties_fn, sep=";")
        with open(dataset_fn, "wb") as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)
        
        if only_gases: #save only startdate and gases columns; can be used, for example, for EPA PMF
            self.df.to_csv(df_fn[:-4]+"_%s.csv"%self.gaslist, sep=",", float_format="%.3f", columns=["startdate"]+self.gases, index=False, na_rep=-999)
            unc_df = pd.DataFrame()
            unc_df["startdate"] = self.df["startdate"]
            for gas in self.gases:
                unc_df.loc[:,gas] = self.properties.loc[gas,"uncertainty"]
            unc_df.to_csv(df_fn[:-4]+"_%s_unc.csv"%self.gaslist, sep=",", float_format="%.3f", index=False, na_rep=-999)
        
        if not only_bin:
            print("\nSaved {0:s},\n      {1:s}, and\n      {2:s}.".format(df_fn, properties_fn, dataset_fn))
        else:
            print("\nSaved {0:s}.".format(dataset_fn))
    
    def initialize_config_info(self):
        """
        Retrieves information from the measurement's configuration file 
        (configfile) (including info on the measurement station) and saves it 
        into the corresponding class variables.
        """
        
        config = pu.read_config_file(self.configfile)
        config_info = config["General Info"]
        self.name = config_info["name"]
        self.timezone = config_info["timezone"]
        self.startdate = pd.to_datetime(config_info["startdate"])
        self.enddate = pd.to_datetime(config_info["enddate"])
        self.folder = "full time range/" if not self.datecut else self.datecut+"/"
        raw = eval(config_info["measured gases"])
        self.gases = list(raw.keys())
        self.num_gases = len(self.gases)
        self.station = Station(config_info["station"])
        
        #initialize self.properties
        for gas in self.gases:
            self.properties.loc[gas,"index"] = raw[gas][0]
            self.properties.loc[gas,"unit"]  = raw[gas][1]
            self.properties.loc[gas,"uncertainty"] = raw[gas][2]
            self.properties.at[gas,"color"] = raw[gas][3]
            self.properties.loc[gas,"short name"] = raw[gas][4]
            self.properties.loc[gas,"name"] = raw[gas][5]
            self.properties.loc[gas,"chemical formula"] = raw[gas][6]
        
        self.properties["unit"] = self.properties["unit"].fillna("")
        
        #add NMF factor names to self.properties
        config_info = config["NMF"]
        raw = eval(config_info["NMF factor properties"])
        for i in range(1,1+12):
            self.properties.loc["_fac%d"%i,"index"] = raw["_fac%d"%i][0]
            self.properties.loc["_fac%d"%i,"name"] = raw["_fac%d"%i][1]
            self.properties.at["_fac%d"%i,"color"] = raw["_fac%d"%i][2]
    
    def initialize_data(self):
        """
        Retrieves all measured data from the datafile and saves it into 
        the corresponding class variables.
        """
        
        print("Retrieving data from %s..."%self.datafile)
        
        self.df = pd.read_csv(self.datafile, delimiter=";", usecols=self.datacolumns, parse_dates=["startdate","enddate"], dayfirst=True)
        self.n_tot_measurements = len(self.df.index)
        self.properties.loc[self.gases,"num_measurements"] = self.n_tot_measurements
    
    def initialize_meteodata(self):
        """
        Reads in meteorological data specified in self.meteofile.
        Calculates averages and standard deviations over each measurement 
        duration (from startdate to enddate). Stores the data in self.df in 
        the METEO_COLUMNS.
        """
        
        meteo_fields = ["time", "WINDGESCHWINDIGKEIT_MITTEL", "WINDRICHTUNG_MITTEL_GRAD", "REL_FEUCHTE_PROZ", "LUFTTEMPERATUR_C", "TAUPUNKTTEMPERATUR_C", "LUFTDRUCK_STATIONSHOEHE_HPA"]
        OWT_fields = ["datetime"] + OWT_COLUMNS
        
        #create new columns in self.df
        for prop in METEO_COLUMNS:
            self.df[prop] = None
        for prop in OWT_fields[1:]:
            self.df[prop] = None
        
        #read in meteo data and OWT data (OWT: objective weather type)
        meteo_df = pd.read_csv(self.meteofile, delimiter=";", usecols=meteo_fields)
        meteo_df = meteo_df.rename(columns=METEO_RENAME_DICT)
        OWT_df = pd.read_csv("data/meteodata/online_wlkvorhersage_2013-2020_en.txt", delimiter="\s+", header=None, usecols=range(len(OWT_fields)), names=OWT_fields)
        OWT_df["datetime"] = pd.to_datetime(OWT_df["datetime"], format="%Y%m%d%H:%M:%S")
        
        year = None
        for index,row in self.df.iterrows():
            startdate = row.startdate_JD
            enddate = row.enddate_JD
            
            if row.startdate.year!=year:
                print("Retrieving meteo data from {0:d}...".format(row.startdate.year))
            year = row.startdate.year
            
            temp_meteo_df = meteo_df.loc[(meteo_df.time > startdate) & (meteo_df.time < enddate)]
            stds = temp_meteo_df.std()
            
            self.df.loc[index,"num_used_meteodata"] = len(temp_meteo_df.index)
            self.df.loc[index,METEO_COLUMNS[1:7]] = temp_meteo_df.mean()
            for ncol in METEO_COLUMNS[7:]:
                self.df.loc[index,ncol] = stds.loc[ncol[:-4]] #get rid of " std"
            
            #retrieve information on OWT (objective weather type)
            date = pu.get_date_from_xlsfloat(round(startdate)-0.5)
            temp_OWT_series = OWT_df.loc[OWT_df.datetime == date].iloc[0]
            for col in OWT_fields[1:]:
                self.df.loc[index,col] = temp_OWT_series[col]
        
        non_float_columns = OWT_COLUMNS+["startdate","enddate"]+list(OWT_LABELS.keys())
        self.df.loc[:, ~self.df.columns.isin(non_float_columns)] = self.df.loc[:, ~self.df.columns.isin(non_float_columns)].astype("float64")
        
        #update self.properties
        self.properties.loc[METEO_COLUMNS,"num_measurements"] = self.df.count().loc[METEO_COLUMNS]
        config_info = pu.read_config_file(self.configfile)["Meteorological data"]
        raw = np.array(list(eval(config_info["meteo properties"]).values()), dtype="object")
        for i,prop in enumerate(METEO_COLUMNS):
            self.properties.loc[prop,"unit"]  = raw[i,0]
            self.properties.loc[prop,"uncertainty"] = raw[i,1]
            self.properties.at[prop,"color"] = tuple(raw[i,2])
            self.properties.loc[prop,"name"] = raw[i,3]
    
    def initialize_additional_info(self):
        """
        For each measurement entry, fills in additional information regarding 
        the components of the OWT, the day of the week, workday/weekend and 
        the wind direction in different precisions (wind direction, 
        prevailing wind direction, precise wind direction).
        """
        
        for prop in list(OWT_LABELS.keys())[2:]:
            self.df[prop] = None
        
        for index,row in self.df.iterrows():
            self.df.loc[index,list(OWT_LABELS.keys())[2]] = row["OWT@t+0h (forecast) label"][:2]  #OWT prevailing wind direction
            self.df.loc[index,list(OWT_LABELS.keys())[3]] = row["OWT@t+0h (forecast) label"][2:4] #OWT cyclonality
            self.df.loc[index,list(OWT_LABELS.keys())[4]] = row["OWT@t+0h (forecast) label"][2]   #OWT cyclonality@950hPa
            self.df.loc[index,list(OWT_LABELS.keys())[5]] = row["OWT@t+0h (forecast) label"][3]   #OWT cyclonality@500hPa
            self.df.loc[index,list(OWT_LABELS.keys())[6]] = row["OWT@t+0h (forecast) label"][-1]  #OWT humidity
        
        self.df["day of the week"] = (self.df["startdate"].dt.round("D")-pd.Timedelta(days=1)).dt.day_name()
        self.df.loc[self.df["day of the week"].isin(WORKDAYS), "workday/weekend"] = "workday"
        self.df.loc[self.df["day of the week"].isin(WEEKEND), "workday/weekend"] = "weekend"
        
        self.df["wind direction"] = self.df["winddirection"].apply(methods.get_wind_dir, args=("normal",))
        self.df["prevailing wind direction"] = self.df["winddirection"].apply(methods.get_wind_dir, args=("prevailing",))
        self.df["precise wind direction"] = self.df["winddirection"].apply(methods.get_wind_dir, args=("precise",))
        
        #set column "date_full_hour" that can be used as the starting time for HYSPLIT simulations
        for index,row in self.df.iterrows():
            date_hh = (row["startdate"]+(row["enddate"]-row["startdate"])/2).round("H")
            self.df.loc[index, "date_full_hour"] = date_hh
    
    def clean_measurements(self, see_statistics=True):
        """
        Cleans up the measurements by replacing all measured concentrations 
        where flag != 0. with value None.
        """
        
        #this blacklist for logged concentrations would lead to the equivalent cleaning performance as the flag!=0 criterion
        #blacklist = [999., 9999., 99999., 999.9, 99.99, 999.99, 999.9, 9999.9, 9999.99]
        n_cleaned = 0
        
        if not self.cleaned:
            counters = []
            for gas in self.gases:
                for index,row in self.df.iterrows():
                    # if row.loc["flag_"+gas] != 0. or row.loc[gas] < 0:
                    if row.loc["flag_"+gas] != 0.: # <==> row.loc[gas] in blacklist
                        n_cleaned += 1
                        self.df.loc[index,gas] = None #values with non-zero flag will be set to None
                        if row.loc[gas] < 0:
                            self.df.loc[index,"flag_"+gas] = -1 #fill flag with -1 to indicate there was a negative value measured here
                        self.properties.loc[gas,"num_measurements"] -= 1
                
                if see_statistics:
                    counter = self.n_tot_measurements-self.properties.loc[gas,"num_measurements"]
                    ratio = float(counter)/self.n_tot_measurements
                    #print("{0:20s}: cleaned {1:4d}/{2:4d} ({3:2.0f}%) logged entries with non-zero flag.".format(
                    print("{0:20s}: cleaned {1:4.0f}/{2:4d} ({3:2.0f}%) logged entries with non-zero flag or negative concentration.".format(
                          gas, counter, self.n_tot_measurements, ratio*100))
                    counters += [counter]
            if see_statistics:
                average = np.mean(counters)
                average_ratio = average/self.n_tot_measurements
                print("Cleaning of data done! Cleaned on average ({0:3.0f} +- {1:.0f})/{2:d} \
                      ({3:2.0f}%) logged entries with non-zero flag.".format(average, 
                      np.std(counters,ddof=1), self.n_tot_measurements, average_ratio*100))
                print("%d measurements cleaned in total"%n_cleaned)
                if self.num_gases==53:
                    average = np.mean(counters[:27])
                    average_ratio = average/self.n_tot_measurements
                    print("For the first 27 gases: Cleaned on average ({0:3.0f} +- {1:.0f})/{2:d} \
                          ({3:2.0f}%) logged entries with non-zero flag.".format(average, 
                          np.std(counters[:27],ddof=1), self.n_tot_measurements, average_ratio*100))
            self.cleaned = True
        else:
            print("clean_measurements(): The dataset is already cleaned, no need to do that again.")
        print("")
    
    def pick(self, listname):
        """
        Updates the list of measured gases under investigation.
        
        Parameters
        ----------
        listname : str or list
            The name of the list of the gases to be picked (must correspond to 
            the ones given in config/config.cfg)
            OR
            a list of strings each containing gas names.

        Raises
        ------
        TypeError
            In case listname is neither of string or list.

        Returns
        -------
        new_list : list
            List (correctly ordered) of updated measured gases that is to be 
            assigned to self.gases.
        """
        
        config_info = pu.read_config_file(self.configfile)["General Info"]
        all_gases = list(eval(config_info["measured gases"]).keys())
        if type(listname)==str: #listname contains name of list of gas names to pick
            new_list = eval(config_info["groups of gases"])[listname]
            new_list.sort(key=lambda x: all_gases.index(x))
        elif type(listname)==list: #listname contains list of gas names to pick
            listname.sort(key=lambda x: all_gases.index(x))
            new_list = listname
        else:
            raise TypeError("pick(): Cannot deal with type of argument 'listname'.")
        
        return new_list
    
    def set_gaslist(self, listname):
        """
        Sets self.measured gases to the given value.
        Caution: The function argument gaslist must be an element returned 
                 by pick()!

        Parameters
        ----------
        gaslist : list
            The list of measured gases to be considered in the following.
        """
        
        gaslist = self.pick(listname)
        self.gases = gaslist
        self.num_gases = len(gaslist)
        self.gaslist = str(listname).replace(": ","-")
        
        print("set_gaslist(): Gases changed to {0:s}.\n".format(str(gaslist)[1:-1]))
    
    def pick_date_interval(self, start=None, end=None):
        """
        Sets the startdate and enddate of the measurements and limits the 
        DataFrame self.df to this interval.
        
        The variable self.datecut is set to a string representing the 
        updated startdate and enddate.

        Parameters
        ----------
        start : string, optional
            Date of the new startdate. The default is None, which leaves the startdate unchanged.
        end : string, optional
            Date of the new enddate. The default is None, which leaves the enddate unchanged.
        """
        
        if start is not None:
            self.startdate = pd.to_datetime(start)
        if end is not None:
            self.enddate   = pd.to_datetime(end)
        
        if start is not None or end is not None:
            self.datecut = str(self.startdate.date())+"_"+str(self.enddate.date())
            indices = self.df.index[(self.startdate <= self.df.startdate.dt.date) & (self.df.startdate.dt.date <= self.enddate)]
            self.df = self.df.loc[indices]
            self.properties["num_measurements"] = self.df.count().loc[self.gases+METEO_COLUMNS] #update number of measurements in self.properties
            self.n_tot_measurements = len(self.df.index)
        
        self.folder = "full time range/" if not self.datecut else self.datecut+"/"
    
    def calc_CMA(self, interval=7):
        """
        Calculates the Central Moving Average for the gas concentrations and 
        all given meteorological parameters and stores the newly retrieved 
        DataFrame in the dictionary self.CMAdf (keys are "%d-day"%interval, 
        where interval is the given argument).
        
        The function uses Gaussian weighting of values around the center. 
        The standard deviation for this Gaussian weight funtion is given by 
        interval/3.
        
        CMA: Central Moving Average
        (TMA: Trailing Moving Average)

        Parameters
        ----------
        interval : int, optional
            The number of days to include in the moving average calculation. 
            The greater the value, the smoother the resulting trends are, 
            but also the more diminished and washed out the details of the 
            trends become.
            The default is 7.
        """
        
        name = "%d-day"%interval
        
        columns = []
        for gas in self.gases:
            columns += [gas]
        for prop in METEO_COLUMNS:
            columns += [prop]
        
        df = self.df[columns]
        
        std = interval/3
        self.CMAdf[name] = df.rolling(interval, min_periods=min(interval,7), win_type="gaussian", center=True).mean(std=std) #Gaussian-distributed weights within one interval, std=interval/3
        # self.CMAdf[name] = df.rolling(interval, min_periods=min(interval,7), center=True).mean() #all concentrations in one interval equally weighted
        self.CMAdf[name]["startdate"] = self.df["startdate"]
        self.CMAdf[name]["enddate"] = self.df["enddate"]
        
        self.CMAdf[name].loc[self.CMAdf[name]["startdate"]=="2017-05-31 00:27:00"] = None
    
    def plot(self, column, df_name=None, frame=None, label="auto", 
             color=None, alpha=1.0, zorder=None, setup=True):
        """
        General plot function to display time series.
        
        By specifying df_name, one can choose to plot CMAs instead of daily values.
        
        Parameters
        ----------
        column : string
            The column's name to be plotted, e.g. gas names or one of the 
            measured meteorological properties.
        df_name : string or None, optional
            The "name" of the DataFrame to be dealt with (the normal one -- 
            self.df -- or one of the CMA DataFrame's stored in self.CMAdf).
            Possible values are None (for self.df) or one of self.CMAdf's keys.
            The default is None (so self.df will be used for plotting).
        frame : tuple of matplotlib figure and ax instances, optional
            The frame of the (possibly to be created) matplotlib figure. 
            A frame is understood as a tuple of a plt.figure() instance and 
            an ax (a plt.subplot() instance).
            The default is None (will take the plt.gcf() and plt.gca()).
        label : string, optional
            The desired label to appear in the plot's legend. 
            The default is "auto", which will automatically generate a suited one.
        color : valid color type, optional
            The color of the plotted line.
            The default is None (will be set to self.properties.loc[column,"color"]).
        alpha : float, optional
            The transparency of the plotted line. Ranges from 0 (fully 
            transparent) to 1 (not transparent).
            The default is 1.0.
        zorder : int, optional
            The zorder of the plotted line (only important when multiple lines 
            in one figure, such as when plotting different CMAs and the daily values).
            The default is None.
        setup : bool, optional
            Boolean whether or not to setup the figure using utils.fig_ax_setup(). 
            Should be set to False when plotting several columns into one 
            figure. Then, the setup can be done after having plotted all considered columns.
            The default is True.
        
        Returns
        -------
        frame:
            The frame (== [fig,ax]) of the (created) matplotlib figure.
        """
        
        df = self.df if df_name is None else self.CMAdf[df_name]
        
        if label=="auto":
            label = "daily" if df_name is None else df_name+" moving average"
        
        if frame is None:
            frame = [plt.gcf(), plt.gca()] #[plt.figure(column),plt.subplot(1,1,1)]
        frame[0].set_size_inches(10, 5)
        
        x = df.startdate
        y = df[column]
        
        if color is None:
            color_key = "daily" if df_name is None else df_name
            color = colors[color_key]
            if label in [None, *self.gases] and column in [*self.gases, *METEO_COLUMNS]: #in case daily is being plotted, but without any label/legend (label=None), OR all gases are plotted in one figure (label=gas)
                color = self.properties.loc[column,"color"]
        
        lw = 1 if "average" not in label else 1.7
        
        pu.plot(x, y, frame=frame, kind="plot",
                marker_option="", color=color, alpha=alpha, 
                label=label, zorder=zorder, lw=lw)
        
        if setup:
            try:
                left = x.loc[1322] if column in self.pick("new gases") else x.loc[0]
            except:
                left = x.iloc[0]
            delta = self.enddate - self.startdate
            sec = pd.Timedelta(days=SEC*delta.days/2140)
            if column in self.pick("new gases"):
                sec *= 0.3
            xlim = (left-sec,x.iloc[-1]+sec)
            try:
                title = self.properties.loc[column, "name"]
            except KeyError:
                title = column
            
            pu.fig_ax_setup(frame, title=title, xlim=xlim, 
                            yscale="linear", legend_position=None)
        
        return frame
    
    def plot_all_timeseries(self, combinations=[(1,0,0,0,0)], plot_gases=True, plot_meteoprops=True, save=save):
        """
        Plots all interesting combinations of daily and moving-average 
        concentraion dataframes. 

        Parameters
        ----------
        combinations : list of lists/tuples, optional
            A list of all desired combinations. Each combination (list entry) 
            is a list or tuple of booleans. The syntax is:
                (daily only without label, daily, 3-day CMA, 7-day CMA, 30-day CMA)
            The default is [(1,0,0,0,0)], which plots only the daily values without a label/legend.
        plot_gases : bool, optional
            Boolean whether or not to plot the gas columns of self.df. 
            The default is True.
        plot_meteoprops : bool, optional
            Boolean whether or not to plot the meteorological columns of self.df. 
            The default is True.
        """
        
        #plot all gases in one figure
        frame_all = plt.subplots(num="all gases")
        for gas in self.gases:
            self.plot(gas, frame=frame_all, setup=False, label=gas, alpha=0.6)
        pu.fig_ax_setup(frame_all, xlabel=xlabel, ylabel=ylabel, 
                        ylim=(3e-2,None), yscale="log", 
                        legend_position="center left", bbox_to_anchor=(1, 0.5), ncol=4)
        if save:
            pu.save_figure(frame_all, out+"timelines/"+self.folder+"#all_gases.png", dpi=DPI)
            print("Saved #all_gases.")
        
        for c in combinations:
            print("\n",c)
            if plot_gases:
                #plot every single gas
                for gas in self.gases:
                    frame = plt.subplots(num=gas)
                    if c[0]:
                        self.plot(gas, label=None) #if plot only "daily"
                    if c[1]:
                        self.plot(gas)
                    if c[2]:
                        self.plot(gas, df_name="3-day", zorder=2)
                    if c[3]:
                        self.plot(gas, df_name="7-day", zorder=3)
                    if c[4]:
                        self.plot(gas, df_name="30-day", zorder=4)
                    pu.fig_ax_setup(frame, xlabel=xlabel, ylabel=ylabel)
                    
                    if save:
                        labelss = frame[1].get_legend_handles_labels()[1]
                        labels = [label[:label.find("y")+1] for label in labelss]
                        directory = self.folder + "+".join(labels)+"/"
                        filename = "%d-%s.png"%(self.properties.loc[gas,"index"], gas)
                        pu.save_figure(frame, out+"timelines/"+directory+filename, dpi=DPI)
                        print("Saved %s."%filename)
                        plt.close("all")
            
            if plot_meteoprops:
                for prop in METEO_COLUMNS:
                    frame = plt.subplots(num=prop)
                    if c[0]:
                        self.plot(prop, label=None) #if plot only "daily"
                    if c[1]:
                        self.plot(prop)
                    if c[2]:
                        self.plot(prop, df_name="3-day", zorder=2)
                    if c[3]:
                        self.plot(prop, df_name="7-day", zorder=3)
                    if c[4]:
                        self.plot(prop, df_name="30-day", zorder=4)
                    pu.fig_ax_setup(frame, xlabel=xlabel, ylabel=prop+" [%s]"%self.properties.unit[prop])
                    if save:
                        labelss = frame[1].get_legend_handles_labels()[1]
                        labels = [label[:label.find("y")+1] for label in labelss]
                        directory = self.folder+"+".join(labels)+"/"
                        pu.save_figure(frame, out+"timelines/"+directory+prop+".png", dpi=DPI)
                        print("Saved %s."%prop)
                        plt.close("all")
    
    def calc_stats(self, basic_stats=(1,1), slope_stats=(1,1)):
        """
        Calculates a set of statistics of the dataset.
        The statistical values are stored in the self.stats DataFrame.
        
        Parameters are booleans whether this statistic is to be calculated 
        (and saved in the output file).
        
        Parameters
        ----------
        basic_stats : bool, optional
            Basic descriptive statistics. Includes mean, std, mean_std, min, 
            max, 05perc, 25perc, median, 75perc, 95perc). 
            The default is True.
        slope_stats : bool, optional
            Statistics of linear regression on the time series of measured 
            concentrations. Includes slope, eslope, nsigma_slope, b, eb, chiq, 
            chiqndof, cov. 
            The default is True.
        """
        
        filename = "results/statistics/descriptive_statistics_"+self.gaslist+"_"+self.folder[:-1]+".txt"
        
        print("calc_stats(): Calculating descriptive statistics, see %s for results.\n"%filename)
        
        #redirect stdout to print to a file
        import sys
        orig_stdout = sys.stdout
        f = open(filename, "w")
        sys.stdout = f
        
        #calculation of basic stats and slope in %/year
        for gas in self.gases:
            # print(gas)
            indices = ~np.isnan(self.df.loc[:,gas])
            if (~indices).all():
                print("\tskipped %s: no values in selected date range"%gas)
                continue
            temp_df = self.df.loc[indices,["startdate_JD",gas]]
            stats = pd.Series()
            
            if basic_stats[0]:
                stats = stats.append(pu.basic_stats(temp_df.loc[:,gas]))
            if slope_stats[0]:
                if gas in self.pick("old gases"):
                    stats = stats.append(methods.slope_stats(temp_df, self.properties.uncertainty[gas])) #for first 27 gases
                elif gas in self.pick("new gases"):
                    stats = stats.append(methods.slope_stats(temp_df, stats.loc["std"])) #for all other gases
                ts = self.CMAdf["30-day"][gas]
                stats["slope %"] = ( ts.iloc[-1]/ts.iloc[0] )**(1./float((self.enddate-self.startdate)/pd.Timedelta(days=365))) - 1
            
            self.stats[gas] = stats
        
        for prop in METEO_COLUMNS:
            indices = ~np.isnan(self.df.loc[:,prop])
            if (~indices).all():
                self.stats[prop] = None
                continue
            temp_df = self.df.loc[indices,["startdate_JD",prop]]
            stats = pd.Series()
            stats.loc["slope %"] = None
            
            if basic_stats[0]:
                stats = stats.append(pu.basic_stats(temp_df.loc[:,prop]))
            if slope_stats[0]:
                stats = stats.append(methods.slope_stats(temp_df, stats.loc["std"]))
                ts = self.CMAdf["30-day"][prop]
                stats["slope %"] = ( ts.iloc[-1]/ts.iloc[0] )**(1./float((self.enddate-self.startdate)/pd.Timedelta(days=365))) - 1
            
            self.stats[prop] = stats
        #end of calculation of basic stats and slope in %/year
        
        #printing all stats (to a file)
        for key in [*self.gases, *METEO_COLUMNS]:
            indices = ~np.isnan(self.df.loc[:,key])
            if (~indices).all():
                continue
            print("statistics for {}:".format(key))
            arr = self.stats[key]
            unit = self.properties.loc[key,"unit"]
            if basic_stats[1]:
                print("\t# measurements  = {0:.0f}".format(self.properties.loc[key,"num_measurements"]))
                print("\tmean            = {0:6.2f} +- {1:6.2f} {2:s}".format(arr["mean"], arr["mean_std"], unit))
                print("\tstd = {0:6.2f} {1:s}".format(arr["std"], unit))
                print("\tunc = {0:6.2f} {1:s}".format(self.properties.loc[key,"uncertainty"], unit))
                print("\tmin             = {0:6.3f} {1:s}".format(arr["min"], unit))
                print("\t5% percentile   = {0:6.2f} {1:s}".format(arr["05perc"], unit))
                print("\t25% percentile  = {0:6.2f} {1:s}".format(arr["25perc"], unit))
                print("\tmedian          = {0:6.2f} {1:s}".format(arr["median"], unit))
                print("\t75% percentile  = {0:6.2f} {1:s}".format(arr["75perc"], unit))
                print("\t95% percentile  = {0:6.2f} {1:s}".format(arr["95perc"], unit))
                print("\tmax             = {0:6.2f} {1:s}".format(arr["max"], unit))
            if slope_stats[1]:
                sign = "+" if arr["slope"]>=0 else "-"
                if arr["chiqndof"]<50:
                    print("\tslope = {0:6.3f} +- {1:.3f} {2:s}/year (nsigma = {3:s}{4:6.2f})".format(arr["slope"], arr["eslope"], unit, sign, arr["nsigma_slope"]))
                else:
                    print("\tslope = {0:6.3f} +- {1:.3f} {2:s}/year".format(arr["slope"], arr["eslope"], unit))
                print("\tb     = {0:6.2f} +- {1:.2f} {2:s}".format(arr["b"], arr["eb"], unit))
                print("\tchiq  = {0:6.2f}".format(arr["chiq"]))
                if key in self.pick("old gases"):
                    print("\tchiq/ndof = {0:6.2f}".format(arr["chiqndof"]))
                elif key in self.pick("new gases"):
                    print("\tchiq/ndof = {0:6.2f} (assumed unc=std)".format(arr["chiqndof"]))
                print("\tslope % = {0:6.3f} %/year".format(arr["slope %"]*100))
        
        sys.stdout = orig_stdout
        f.close()
    
    def fft(self, gas):
        """
        A fast Fourier transformation of the time series of one specific gas.
        This functionality has not been developed to detail.

        Parameters
        ----------
        gas : string
            The gas the fft is done for.
        """
        
        print("\n%s"%gas)
        
        import scipy as sc
        
        start = int(round(self.df.startdate_JD.iloc[0])) #.date()
        end   = int(round(self.df.startdate_JD.iloc[-1])) #.date()
        
        df = self.df.set_index("startdate_JD")
        
        #remove duplicate measurements (two measurements per night)
        df.index = df.index.to_series().apply(lambda x: np.round(x,0))
        dupls = df.index.duplicated(keep="first")
        df = df[~dupls]
        
        t = df.index
        r = pd.Index(range(start,end), name=t.name)
        df = df.reindex(t.union(r)).interpolate("index")
        # plt.scatter(df.index, df[gas])
        
        self.newdf = df
        
        temp_fft = sc.fftpack.fft(np.array(self.newdf[gas]), n=10000)
        temp_psd = np.abs(temp_fft) ** 2
        fftfreq = sc.fftpack.fftfreq(len(temp_psd), 1. / 365.25)
        
        i = fftfreq > 0
        
        fig, ax = plt.subplots(1, 1, figsize=(8, 4))
        ax.plot(fftfreq[i], temp_psd[i])
        ax.set_xlim(0, 2.5)
        ax.set_xlabel("Frequency (1/year)")
        ax.set_title(gas)
        # ax.set_yscale("log")
    
    def boxplots_conc(self, path=None, save=False):
        """
        Creates one boxplot for each gas visualizing its distribution of 
        measured concentrations and display all in one figure.
        """
        
        #whis = [0,100]
        whis = [0.27,99.73] #3sigma environment
        
        fig_sizex = 14.*(self.num_gases/52)**0.7
        fig = plt.figure(figsize=(fig_sizex,7))
        ax = self.df.boxplot(column=self.gases, 
                             rot=90, whis=whis, showfliers=False)
        plt.xticks(range(1,self.num_gases+1), self.properties.loc[self.gases,"short name"].values)
        
        pu.fig_ax_setup([fig,ax], ylabel="mixing ratio [ppt]", yscale="log", 
                        legend_position=None)
        if save and path is not None:
            filename = "boxplots_" + self.gaslist
            pu.save_figure(fig, filename=path+self.folder+filename, dpi=DPI)
    
    def boxplots(self, by, verbose=True, plot_boxplots=True, sort=True, path=None, save=False):
        """
        Creates (sorted) boxplots for each of the gases grouped by a certain 
        column specified by the argument by.
        
        Parameters
        ----------
        by : string
            The column to group the gas concentrations by.
        verbose : bool, optional
            Whether or not to be verbose and print strong deviations from the 
            mean in the console. The default is True.
        plot_boxplots : bool, optional
            Whether or not to display the boxplots in a figure. Can be set to 
            False if only the console output is needed. The default is True.
        sort : bool, optional
            Whether or not to sort the boxplots for each gas by their median. 
            The default is True.
        path : string, optional
            The path to save the plots in. The default is None.
        """
        
        print("creating boxplots grouped by",by.replace(" label","..."))
        
        index = OWT_LABELS[by] if "OWT" in by else LABELS[by]
        nobs_empty = pd.Series(0, index=index)
        minnobs = 10 #0.8 * 1./40 * self.properties.num_measurements[gas] #10
        lower = 0.25
        upper = 1-lower
        
        count_up = pd.DataFrame(0, columns=self.gases, index=index)
        count_down = count_up.copy()
        
        for gas in self.gases:
            nobs = self.df.loc[~self.df[gas].isna(),by].value_counts()
            nobs = nobs.add(nobs_empty, fill_value=0).astype(int)
            #find strong deviations from median of gas concentrations
            MEDIAN = np.nanmedian(self.df[gas])
            LQ = np.nanquantile(self.df[gas], lower)
            UQ = np.nanquantile(self.df[gas], upper)
            
            #make sure no NaNs in the grouped object (neither NaNs in gas' concentration nor in by's column)
            df_nona = self.df.loc[(~self.df[gas].isna()) & (~self.df[by].isna()),[gas,by]]
            grouped_nona = df_nona.groupby(by)
            
            medians = grouped_nona[gas].median()
            lqs = grouped_nona[gas].quantile(lower)
            uqs = grouped_nona[gas].quantile(upper)
            
            if verbose:
                for label,lq,med,uq in zip(lqs.index,lqs,medians,uqs):
                    if nobs[label]>=minnobs:
                        #study *up* deviations
                        if med>1.5*MEDIAN: #1.8 #1.5
                            print("\tstrong dev. in {0:19s} vs. {1:9s} (nobs={2:3d}): med {3:7.2f} = {4:.1f}*med_of_gas {5:7.2f} (up)".format(gas,label,nobs[label],med,med/MEDIAN,MEDIAN))
                            count_up.loc[label,gas] += 1
                        if lq>1.1*MEDIAN: #1.6 #1.1
                            print("\tstrong dev. in {0:19s} vs. {1:9s} (nobs={2:3d}):  lq {3:7.2f} = {4:.1f}*med_of_gas {5:7.2f} (up)".format(gas,label,nobs[label],lq,lq/MEDIAN,MEDIAN))
                            count_up.loc[label,gas] += 1
                        if lq>1.0*UQ: #1.4 #1.0
                            print("\tstrong dev. in {0:19s} vs. {1:9s} (nobs={2:3d}):  lq {3:7.2f} = {4:.1f}* uq_of_gas {5:7.2f} (up)".format(gas,label,nobs[label],lq,lq/UQ,UQ))
                            count_up.loc[label,gas] += 1
                            
                        #study *down* deviations
                        # if med<0.98*MEDIAN: #0.5 #0.7
                        #     print("\tstrong dev. in {0:19s} vs. {1:9s} (nobs={2:3d}): med {3:7.2f} = {4:.1f}*med_of_gas {5:7.2f} (down)".format(gas,label,nobs[label],med,med/MEDIAN,MEDIAN))
                        #     count_down.loc[label,gas] = 1
                        # if uq<0.9*MEDIAN: #0.7 #0.9
                        #     print("\tstrong dev. in {0:19s} vs. {1:9s} (nobs={2:3d}):  uq {3:7.2f} = {4:.1f}*med_of_gas {5:7.2f} (down)".format(gas,label,nobs[label],uq,uq/MEDIAN,MEDIAN))
                        #     count_down.loc[label,gas] = 1
                        # if uq<1.0*LQ: #1.0 #1.0
                        #     print("\tstrong dev. in {0:19s} vs. {1:9s} (nobs={2:3d}):  uq {3:7.2f} = {4:.1f}* lq_of_gas {5:7.2f} (down)".format(gas,label,nobs[label],uq,uq/LQ,LQ))
                        #     count_down.loc[label,gas] = 1
            
            if plot_boxplots:
                grouped = self.df.groupby(by) #to also include NaNs here (so that they show up in the plots [without any boxplot])
                df = pd.DataFrame({col:vals[gas] for col,vals in grouped})
                
                if sort: #sort by descending median
                    meds = df.median()
                    meds.sort_values(ascending=False, inplace=True)
                    df = df[meds.index]
                
                figwidth = 15 if len(nobs.index)>8 else 5+len(nobs.index)
                fig,ax = plt.subplots(figsize=(figwidth,9))
                lw = 2
                boxprops = dict(linewidth=lw, color="C0")
                medianprops = dict(linewidth=lw, color="limegreen")
                whiskerprops = dict(linewidth=lw, color="C0")
                capprops = dict(linewidth=lw)
                df.boxplot(ax=ax, rot=90, whis=[0,100], boxprops=boxprops, 
                           medianprops=medianprops, whiskerprops=whiskerprops, capprops=capprops)
                
                #show number of observations in plot
                maxs = grouped[gas].max()
                for tick,label in zip(range(len(index)),ax.get_xticklabels()):
                    y = maxs.loc[label.get_text()] + 0.015*(ax.get_ylim()[1]-ax.get_ylim()[0])
                    if y==y: #avoid y==NaN and thus "posx and posy should be finite values" warning
                        ax.text(tick+1, y, nobs.loc[label.get_text()], #nobs[tick],
                                ha="center", fontsize=10, color="C0", weight="semibold", 
                                bbox=dict(boxstyle="round", facecolor="wheat", alpha=1.0, pad=0.1))
                
                #horizontal lines in plot
                ax.axhline(UQ, alpha=0.7, color="orange", ls="--", lw=2, zorder=1, label="{0:.0f}%/{1:.0f}% quantiles".format(lower*100,upper*100))
                ax.axhline(MEDIAN, alpha=0.7, color="red", ls="--", lw=2, zorder=1, label="median = %.2f ppt"%MEDIAN)
                ax.axhline(LQ, alpha=0.7, color="orange", ls="--", lw=2, zorder=1)
                
                title = self.properties.loc[gas,"name"]+", grouped by "+by.replace(" label","")
                if by=="OWT@t+0h (forecast) label":
                    title = self.properties.loc[gas,"name"]+", grouped by "+OWTlabel
                
                pu.fig_ax_setup([fig,ax], title=title, 
                                xlabel="", ylabel="mixing ratio [ppt]", yscale="linear")
                
                if save and path is not None:
                    full_path = path+self.folder+"grouped by "+by.replace(" label","").replace("/","-")+"/"
                    if not sort: #insert "unsorted/" in path
                        ind = full_path.find("grouped by")
                        full_path = full_path[:ind]+"unsorted/"+full_path[ind:]
                    filename = "boxplots_%s"%gas
                    pu.save_figure(fig, filename=full_path+filename, dpi=DPI)
                    plt.close("all")
        
        for df,name_df in zip([count_up,count_down],["count_up","count_down"]):
            df.loc["total"] = df.sum()
            df.loc[:,"total"] = df.sum(axis=1)
            df.loc[:,"number of occurence"] = nobs.astype("int")
            df = df.sort_values("total",ascending=False)
            df.to_csv("results/boxplots/"+name_df+" "+by.replace(" label","").replace("/","-")+".csv",sep=";")
    
    def calc_deviation_counts(self, by, gases=None, mode="quartile"):
        """
        Calculates the fraction of measurements for each gas (or a group of 
        gases) that lies within the lower and upper quartile or half and 
        stores this information in csv-files in the results folder.

        Parameters
        ----------
        by : string, optional
            The column to group the gas concentrations by.
        gases : list, optional
            A list of gas names to be considered. The default is None, which 
            will translate into self.gases.
        mode: string, optional
            Defines whether the elements in the lower/upper *quartile* or *half* should be selected.
            Can be either "quartile" or "half".
        """
        
        if gases is None:
            gases = self.gases
        
        index = OWT_LABELS[by] if "OWT" in by else LABELS[by]
        
        num = pd.DataFrame(0, columns=gases, index=index)
        num_up = num.copy()
        num_down = num.copy()
        
        if mode=="quartile":
            lower = 0.25
        elif mode=="half":
            lower = 0.50
        upper = 1-lower
        
        for gas in gases:
            df = pd.DataFrame({col:vals[gas] for col,vals in self.df.groupby(by)})
            LQ = np.nanquantile(self.df[gas], lower)
            UQ = np.nanquantile(self.df[gas], upper)
            
            num.loc[:,gas] = df.count()
            num_up.loc[:,gas] = df[df>UQ].count()
            num_down.loc[:,gas] = df[df<LQ].count()
        
        frac_up = num_up/num
        frac_down = num_down/num
        
        for df,df_name in zip([num,num_up,frac_up,num_down,frac_down],["num","num_up","frac_up","num_down","frac_down"]):
            df.loc["total"] = df.sum()
            df.loc[:,"total"] = df.sum(axis=1)
            #df = df.sort_values("total/number of measurements with each OWT", ascending=False)
            df.to_csv("results/deviation_counts/"+df_name+".csv",sep=";")
    
    def show_deviation_counts(self, by, gases=None, mode="quartile", path=None, save=False):
        """
        Displays the fraction of measurements for each gas (or a group of 
        gases) that lies within the lower and upper quartile or half in 
        a correlation matrix-style plot. Retrieves the displayed information 
        from the csv-files in the results folder created by self.calc_correlation().

        Parameters
        ----------
        by : string, optional
            The column to group the gas concentrations by. Must match what was 
            given in calc_correlation().
        gases : list, optional
            A list of gas names to be considered. The default is None, which 
            will translate into self.gases. Must match what was 
            given in calc_correlation().
        mode: string, optional
            Defines whether the elements in the lower/upper *quartile* or *half* should be selected.
            Can be either "quartile" or "half". Must match what was given in 
            calc_correlation().
        """
        
        if gases is None:
            gases = self.gases
        
        index = OWT_LABELS[by] if "OWT" in by else LABELS[by]
        
        num = pd.read_csv("results/"+"num.csv", sep=";", index_col=0).loc[index,gases]
        num_up = pd.read_csv("results/"+"num_up.csv", sep=";", index_col=0).loc[index,gases]
        frac_up = pd.read_csv("results/"+"frac_up.csv", sep=";", index_col=0).loc[index,gases]
        num_down = pd.read_csv("results/"+"num_down.csv", sep=";", index_col=0).loc[index,gases]
        frac_down = pd.read_csv("results/"+"frac_down.csv", sep=";", index_col=0).loc[index,gases]
        
        #calculate Poisson counting uncertainties
        unc_num_up = num_up.pow(0.5)
        unc_num_down = num_down.pow(0.5)
        
        minnobs = 20
        N = 2 #no. of stds
        
        if mode=="quartile":
            minfrac = 0.25
        elif mode=="half":
            minfrac = 0.50
        
        frac_up = frac_up[((num_up-N*unc_num_up)/num>=minfrac) & (num>minnobs)]
        frac_down = frac_down[((num_down-N*unc_num_down)/num>=minfrac) & (num>minnobs)]
        
        LISTE = [[frac_up,num_up],[frac_down,num_down]]
        for ind,liste in enumerate(LISTE):
            frac = liste[0]
            dev = liste[1]
            fig = plt.figure(figsize=(40*53/len(gases), 20*40/len(index)))
            
            im = plt.matshow(frac, fignum=fig.number, vmin=minfrac, vmax=frac.max().max(), zorder=1)
            # im = plt.matshow(frac, fignum=fig.number, zorder=1)
            
            if ind==0:
                if mode=="quartile":
                    plt.title("Fraction of measurements at each OWT that are in the 4th quartile (75-100%) of all measurements of this corresponding gas.\n \
                              Shown are only those fractions that are at least two standard deviations bigger than 0.25 (= expected fraction average of this fraction over all OWTs).", y=1.15)
                elif mode=="half":
                    plt.title("Fraction of measurements at each OWT that are in the upper half (50-100%) of all measurements of this corresponding gas.\n \
                              Shown are only those fractions that are at least two standard deviations bigger than 0.25 (= expected fraction average of this fraction over all OWTs).", y=1.15)
            elif ind==1:
                if mode=="quartile":
                    plt.title("Fraction of measurements at each OWT that are in the 1st quartile (0-25%) of all measurements of this corresponding gas.\n \
                              Shown are only those fractions that are at least two standard deviations bigger than 0.25 (= expected fraction average of this fraction over all OWTs).", y=1.15)
                elif mode=="half":
                    plt.title("Fraction of measurements at each OWT that are in the lower half (0-50%) of all measurements of this corresponding gas.\n \
                              Shown are only those fractions that are at least two standard deviations bigger than 0.25 (= expected fraction average of this fraction over all OWTs).", y=1.15)
            
            plt.xticks(range(len(frac.columns)), frac.columns, fontsize=14, rotation=90)
            plt.yticks(range(len(frac.index)), frac.index, fontsize=14)
            plt.ylabel(by, fontsize=18)
            
            #colorbar
            im_ratio = frac.shape[0]/frac.shape[1]
            cbar = plt.colorbar(im, fraction=0.0462*im_ratio)
            cbar.ax.tick_params(labelsize=14)
            
            #annotations
            color = "black"
            for i in range(len(gases)):
                for j in range(len(index)):
                    if frac.iloc[j,i]>=minfrac:
                        plt.text(i, j, "{0:.0f}\n/{1:.0f}".format(dev.iloc[j,i],num.iloc[j,i]),
                                 ha="center", va="center", color=color, 
                                 fontsize=1.2*0.75*12.*53/len(gases))
            
            if save and path is not None:
                by = by.replace(" label","").replace("/","-")
                filename = "fraction_of_measurements_in_"
                if mode=="quartile":
                    filename += "upper_quartile" if ind==0 else "lower_quartile"
                elif mode=="half":
                    filename += "upper_half" if ind==0 else "lower_half"
                filename += "_of gas (by %s)_%s"%(by,self.gaslist)
                pu.save_figure(fig, filename=path+self.folder+filename, dpi=4*max(len(gases),len(index)))
    
    def correlation_matrix(self, columnsx=None, columnsy=None, 
                          scale="linear", path=None, save=False):
        """
        Creates a correlation matrix containing the correlaation indices 
        between all columns contained in columnsx and columnsy.
        
        Parameters
        ----------
        columnsx : array-like, optional
            The columns of the dataframe (i.e. the gases and/or meteorological 
            parameters) that will be shown on the horizontal axis in the 
            correlation matrix visualisation. 
            The default is None (will be set to self.gases).
        columnsy : array-like, optional
            The columns of the dataframe (i.e. the gases and/or meteorological 
            parameters) that will be shown on the vertical axis in the 
            correlation matrix visualisation. 
            The default is None (will be set to self.gases).
        scale : string, optional
            Affects colorbar scaling. Either "linear" or "log". 
            The default is "linear".
        """
        
        if columnsx is None:
            columnsx = self.gases
        if columnsy is None:
            columnsy = self.gases
        
        columns = columnsx if columnsx==columnsy else columnsx+columnsy
        df = self.df[columns]
        
        corr = df.corr()
        corr = corr.loc[columnsy,columnsx]
        
        fig = plt.figure(figsize=(20*(len(columnsx)/27)**0.5, 15*(len(columnsy)/27)**0.5))
        
        import matplotlib
        orig_cmap = matplotlib.cm.PiYG
        vmin,vmax = corr.min().min(),corr.max().max()
        # absmax = max(abs(corr.min().min()),corr.max().max())
        # vmin,vmax = -absmax,absmax
        # vmin,vmax = -0.3,0.3
        vmin,vmax = -1,1
        mp = 1 - vmax/(vmax+abs(vmin)) #to center the white part of the colormap at 0
        cmap = pu.shiftedColorMap(orig_cmap, midpoint=mp)
        
        if scale=="log":
            from matplotlib.colors import LogNorm,SymLogNorm
            im = plt.matshow(corr, fignum=fig.number, norm=SymLogNorm(linthresh=0.01, vmin=vmin, vmax=vmax), zorder=1, cmap=orig_cmap)
        elif scale=="linear":
            im = plt.matshow(corr, fignum=fig.number, vmin=vmin, vmax=vmax, zorder=1, cmap=cmap)
        ax = plt.gca()
        
        xticklabels = self.properties.loc[columnsx, "short name"] if not any(self.properties.loc[columnsx, "short name"].isna()) else self.properties.loc[columnsx, "name"]
        yticklabels = self.properties.loc[columnsy, "short name"] if not any(self.properties.loc[columnsy, "short name"].isna()) else self.properties.loc[columnsy, "name"]
        plt.xticks(range(len(corr.columns)), xticklabels, fontsize=14, rotation=90)
        plt.yticks(range(len(corr.index)), yticklabels, fontsize=14)
        
        #colorbar
        im_ratio = corr.shape[0]/corr.shape[1]
        cbar = plt.colorbar(im, fraction=0.0462*im_ratio)
        if scale=="log":
            lower = -1. if vmin<=-0.7 else vmin*1.2
            upper = 1. if vmax>=0.7 else vmax*1.2
            arr = [i for i in [-1,-0.5,-0.1,-0.05,-0.01,0,0.01,0.05,0.1,0.5,1] if i >= lower]
            cbar.set_ticks(arr)
            cbar.set_ticklabels(arr)
            plt.clim(lower,upper)
        cbar.ax.tick_params(labelsize=14)
        
        #annotations (display correlation indices in each cell of plot)
        for i in range(len(columnsx)):
            for j in range(len(columnsy)):
                c = corr.iloc[j,i]
                if c==c: #avoid corr==NaN
                    plt.text(i, j, "{:.2f}".format(c),
                             ha="center", va="center", color="black", 
                             fontsize=1.2*0.75*12*(27/len(columnsx))**0.5)
        
        if save and path is not None:
            fnamedict = { "gases" : self.gases[0], 
                          "meteodata" : METEO_COLUMNS[1:7][0], 
                          "meteodatastd" : METEO_COLUMNS[7:][0], 
                } #only needed for correct naming of the plot file
            x,y = [list(fnamedict.keys())[list(fnamedict.values()).index(col[0])] for col in [columnsx,columnsy]]
            filename = "correlation_matrix_between_" + x + "_and_" + y + "_" + self.gaslist
            pu.save_figure(fig, filename=path+self.folder+filename, dpi=100*max(len(columnsx),len(columnsy))/10)
    
    def scatter_matrix(self, columns=None, path=None, save=False):
        """
        Creates scatter plots of each column with each column that are contained 
        in columns. Gets very computation-intensive with increasing number of 
        columns in columns. Also creates a linear regression in each of the 
        scatter plots.
        
        Parameters
        ----------
        columns : list, optional
            List of columns of self.df that are to be included in the scatter plot 
            matrix. The default is None (will use self.gases).
        """
        
        import seaborn as sns
        
        if columns==None:
            columns = self.gases
        df = self.df.loc[:,columns]
        
        sns.pairplot(df, kind="reg", 
                     plot_kws={"scatter_kws":{"s": 10, "alpha": 0.05}}, 
                     diag_kws={"bins": 20})
        
        if save and path is not None:
            filename = "pairplot_" + self.gaslist
            pu.save_figure(plt.gcf(), filename=path+self.folder+filename, dpi=DPI)
    
    def windroses(self, criterion_value=0.75, exclude_calm_winds=1.0, path=None, save=False):
        """
        Creates windroses of the given gas concentration measurements for all gases. 
        Excludes rows that have a NaN in the winddirection and excludes each 
        gas measurement that is a NaN. 

        Parameters
        ----------
        criterion_value : float, optional
            Displayed wind speed measurements must lie in the (100*criterion_value)th 
            quantile in order to be included in the windrose. The default is 0.75.
        exclude_calm_winds : float, optional
            Measurements at wind speeds of exclude_calm_winds m/s or lower 
            are excluded. The higher this value, the more significant the 
            direction may be, but one also loses statistics. 
            The unit is meters/second (m/s). The default is 1.0.
        """
        
        # import windrose as wr
        import windrose_custom as wr # Despite a couple of lines (regarding the desired wind direction sectors), this module is equivalent to the windrose package. Full credit to the developers of the windrose package.
        import matplotlib.cm as cm
        
        #exclude NaNs in winddirection
        df = self.df.loc[~self.df["winddirection"].isna()]
        
        #exclude calm winds if wanted
        if exclude_calm_winds is not None:
            df = df[df["windspeed"] > exclude_calm_winds]
        df = df[self.gases+["winddirection"]]
        
        for gas in self.gases:
            #exclude NaNs in concentrations of gas
            df_gas = df.loc[~df[gas].isna(), [gas,"winddirection"]]
            
            #select concentrations above criterion_value
            criterion_conc = np.quantile(df_gas[gas], criterion_value)
            df_gas = df_gas.loc[df_gas[gas] > criterion_conc]
            
            wd = df_gas["winddirection"].values
            concs = df_gas[gas].values
            
            rmax = 60 if gas in self.pick("old gases") else 10
            rmax *= 4 if criterion_value==0 else 1
            
            #draw windrose
            fig = plt.figure(gas, figsize=(8,8))
            ax = wr.WindroseAxes.from_ax(fig=fig, rmax=rmax, theta_labels=THETA_LABELS)
            cmap = pu.truncate_colormap(cm.inferno, minval=0.1, maxval=0.9)
            ax.contour(wd, concs, bins=6, cmap=cmap)
            
            dec_pl = 1 if criterion_conc>1. else 2
            ax.set_legend(units="ppt", decimal_places=dec_pl)
            
            title = "wind directional frequency,\nmeasurements above {0:s} percentile".format(pu.ordinal(criterion_value*100)) if criterion_value>0 else "wind directional frequency, all measurements"
            title += " of {0:s} (median = {1:.2f} ppt)".format(gas, np.nanmedian(df[gas]))
            plt.xticks(weight="semibold")
            pu.fig_ax_setup([fig,ax], title=title)
            
            #print some info
            # print("bins",ax._info["bins"])
            # print("table",ax._info["table"])
            
            if save and path is not None:
                full_path = path+"criterion_value_%.2f/"%criterion_value
                pu.save_figure(fig, filename=full_path+self.folder+"windrose_%s"%gas, dpi=DPI)
                plt.close("all")
    
    def windroses_CPF(self, criterion_value=0.75, exclude_calm_winds=1.0, exclude_low_occurances=5, path=None, save=False):
        """
        Computes the conditional probability function (CPF) for each 
        wind direction for each gas in the dataset and plots them in a 
        wind rose for each gas.

        Parameters
        ----------
        criterion_value : float, optional
            The wind speed at a given time of measurement must lie in the 
            (100*criterion_value)th quantile in order to be taken as a 
            high wind speed. The default is 0.75, so the upper quartile CPF 
            is being displayed in the wind rose.
        exclude_calm_winds : float, optional
            Measurements at wind speeds of exclude_calm_winds m/s or lower 
            are excluded. The higher this value, the more significant the 
            direction may be, but one also loses statistics. 
            The unit is meters/second (m/s). The default is 1.0.
        exclude_low_occurances : int, optional
            Computed CPF values if statistics were too low. The number of 
            occurances of a given wind direction (sector) observation must 
            be greater than or equal to exclude_low_occurances. The default is 5.
        """
        
        # import windrose as wr
        import windrose_custom as wr # Despite a couple of lines (regarding the desired wind direction sectors), this module is equivalent to the windrose package. Full credit to the developers of the windrose package.
        
        by = "precise wind direction"
        nsector = 16
        angles = np.arange(0, 2*np.pi + 2*np.pi/nsector, 2*np.pi/nsector)
        
        #exclude NaNs in winddirection
        df = self.df.loc[~self.df[by].isna()]
        
        #exclude calm winds if wanted
        if exclude_calm_winds is not None:
            df = df[df["windspeed"] > exclude_calm_winds]
        df = df[self.gases+[by]]
        
        for gas in self.gases:
            #exclude NaNs in concentrations of gas
            df_gas = df.loc[~df[gas].isna(), [gas,by]]
            
            #no. of observations for this gas in each wind sector
            n = df_gas.groupby(by).size().reindex(THETA_LABELS, fill_value=0)
            
            #select concentrations above criterion_value
            criterion_conc = np.quantile(df_gas[gas], criterion_value)
            df_gas = df_gas.loc[df_gas[gas] > criterion_conc]
            
            #no. of observations above criterion_value
            m = df_gas.groupby(by).size().reindex(THETA_LABELS, fill_value=0)
            
            CPF = m.divide(n).fillna(0.) #NaNs can occur if n==0, so they get replaced with 0
            CPF[n < exclude_low_occurances] = 0.0 #arbitrary, but thus exclude CPF values based on very low statistics
            CPF.loc["new"] = CPF[CPF.index[0]] #to close the windrose loop
            
            #draw windrose
            fig = plt.figure("CPF {0:s}, exclude windspeed<{1:.1f} m/s".format(gas, exclude_calm_winds), figsize=(8,8))
            ax = wr.WindroseAxes.from_ax(fig=fig, theta_labels=THETA_LABELS)
            patch = ax.fill(angles, CPF, facecolor=(0,0,0,0.2), edgecolor="black", lw=2.5, zorder=2)
            rmax = 0.6 if gas in self.pick("old gases") else 0.8
            ax.set_rmax(rmax)
            
            #draw ring at expected CPF; discarded because expected CPF != 1-criterion_value due to non-uniform distribution of wind speeds
            if criterion_value>0:
                theta = np.arange(1000)*2*np.pi/1000
                # exp_CPF = (1-criterion_value)*np.ones(1000)
                exp_CPF = m.sum()/n.sum() * np.ones(1000)
                ax.plot(theta, exp_CPF, "crimson", alpha=0.7, zorder=1)
                ax.set_rmax(rmax)
            
            #add number of observations to plot
            for index,angle,num in zip(CPF.index,angles,n):
                thetapos = angle if index!="E" else -0.04
                ax.annotate(num, xy=(thetapos, 0.96*ax.get_ylim()[1]),  #theta, radius
                            color="crimson", weight=650, ha="center", va="center", 
                            bbox=dict(boxstyle="round", facecolor="wheat", alpha=1.0, pad=0.1))
            
            title = "wind directional frequency,\nCPF of measurements above {0:s} percentile".format(pu.ordinal(criterion_value*100)) if criterion_value>0 else "wind directional frequency, all measurements"
            title += " of {0:s}".format(gas)
            plt.xticks(weight="semibold")
            pu.fig_ax_setup(fig, title=title)
            
            #print some info
            print("\n%s:"%gas)
            # print("m",m)
            # print("n",n)
            # print("CPF",CPF)
            # print("max(CPF):       {0:.3f} (in {1:3s} direction, with {2:3d} observations)".format(CPF.max(),CPF.idxmax(),n[CPF.idxmax()]))
            
            #print highest CPF values
            n_min = 35
            CPF = CPF.drop("new")
            CPF_n_min = CPF[n>n_min]
            n_n_min = n[n>n_min]
            print("max(CPF,n>{3:d}):  {0:.3f} (in {1:3s} direction, with {2:3d} observations)".format(CPF_n_min.max(),CPF_n_min.idxmax(),n_n_min[CPF_n_min.idxmax()],n_min))
            n_n_min = n_n_min.drop(CPF_n_min.idxmax())
            CPF_n_min = CPF_n_min.drop(CPF_n_min.idxmax())
            print("max2(CPF,n>{3:d}): {0:.3f} (in {1:3s} direction, with {2:3d} observations)".format(CPF_n_min.max(),CPF_n_min.idxmax(),n_n_min[CPF_n_min.idxmax()],n_min))
            # print(m.sum(),n.sum(),m.sum()/n.sum())
            # print("n",n)
            
            # print("bins",ax._info["bins"])
            # print("table",ax._info["table"])
            
            if save and path is not None:
                full_path = path+"CPF/criterion_value_%.2f/"%criterion_value
                pu.save_figure(fig, filename=full_path+self.folder+"windrose_%s"%gas, dpi=DPI)
                plt.close("all")
    
    def PCA(self, n_components=2, path=None, save=False):
        """
        Principal Component Analysis.

        Parameters
        ----------
        n_components : int, optional
            Number of principal components the PCA is supposed to calculate. 
            The default is 2.
        """
        
        #fill NaN values with column medians
        # df = self.CMAdf["3-day"][self.gases] #3-day, 7-day, or 30-day CMA
        df = self.df[self.gases]
        x = df.fillna(df.median())
        #x = self.df[self.gases].dropna() #attention: often retains only a few of the total number of measurements
        
        x = StandardScaler().fit_transform(x).transpose()
        
        #perform PCA
        pca = skldec.PCA(n_components=n_components)
        principalComponents = pca.fit_transform(x)
        principalDf = pd.DataFrame(data = principalComponents, 
                                   columns = ["principal component %d"%(i+1) for i in range(n_components)],
                                   index = self.gases)
        
        print("PCA: explained variance ratio =", pca.explained_variance_ratio_)
        
        #plot PCA
        fig,ax = plt.subplots(figsize=(8,8))
        pu.plot(principalDf["principal component 1"], principalDf["principal component 2"],
                frame=[fig,ax], marker_option="o")
        pu.fig_ax_setup([fig,ax], 
                        xlabel="Principal Component 1 (%.1f%%)"%(pca.explained_variance_ratio_[0]*100),ylabel="Principal Component 2 (%.1f %%)"%(pca.explained_variance_ratio_[1]*100), 
                        legend_position=None)
        texts = []
        for i, gas in enumerate(self.gases):
            txt = self.properties.loc[gas, "short name"]
            texts += [ax.annotate(txt, principalDf.iloc[i,:2], fontsize=11, ha="center")]
        adjust_text(texts, expand_text=(1.001, 1.001))
        
        if save and path is not None:
            filename = "PCA_" + self.gaslist
            pu.save_figure(fig, filename=path+self.folder+filename, dpi=DPI)
    
    def NMF(self, n_factors=8, solver="cd", beta_loss="kullback-leibler", normed=False):
        """
        Non-negative matrix factorization. The data (df) is decomposed 
        (factorized) into a matrix product W*H that is supposed to 
        approximate the given data as accurately as possible. 
        The resulting dimensions are:
            dim(df) = ( self.n_tot_measurements, len(self.gases) )
            dim(W)  = ( self.n_tot_measurements, n_factors )
            dim(H)  = ( n_factors, len(self.gases) )
        The result is stored in an NMF container object within the dictionary self.NMFs, 
        with the corresponding key being of the form
            "<gaslist>_<solver>(_<beta_loss> [if solver=="mu"])_<n_factors> factors(_normed [if normed])"
        
        For more information, see documentation of the class NMF.

        Parameters
        ----------
        n_factors : int, optional
            The number of factors. This will be the number of columns of W 
            and the number of rows of H. The default is 8.
        solver : string, optional
            The used solver within the sklearn.decomposition.NMF method.
            One can choose from coordinate descent ("cd") and multiplicative 
            update solvers ("mu"). For multiplicative update solving, the 
            desired beta_loss function must be provided.
            The default is "cd".
        beta_loss : string, optional
            The used beta_loss function. Only applies in case of 
            multiplicative update solving. Possible values are 
            "frobenius" or "kullback-leibler" or "itakura-saito". 
            The default is "kullback-leibler", which showed to result in 
            most accurate approximations of the given data within 
            multiplicative update solving.
        normed : bool, optional
            Boolean whether or not to norm all concentration data by dividing 
            through the species' medians before executing the NMF.
            The default is False.
        """
        
        if type(n_factors) != list:
            n_factors = [n_factors]
        
        for n_fac in n_factors:
            if solver=="mu":
                print("\nNMF ({solv}, {bl}, {n}):".format(solv=solver,bl=beta_loss,n=n_fac))
            elif solver=="cd":
                print("\nNMF ({solv}, {n}):".format(solv=solver,n=n_fac))
            
            columns = self.gases
            # df = self.CMAdf["3-day"][columns] #3-day, 7-day, or 30-day
            df = self.df[columns]
            
            #handle NaNs
            df = df.fillna(self.df.median())
            
            if normed:
                df /= df.median()
            
            # perform NMF
            max_iter = 100000 #10000
            if solver=="cd":
                model = skldec.NMF(n_components=n_fac, solver=solver, max_iter=max_iter)
            elif solver=="mu":
                model = skldec.NMF(n_components=n_fac, solver=solver, beta_loss=beta_loss, max_iter=max_iter)
            W = model.fit_transform(df)
            H = model.components_
            
            #transform W and H into pandas DataFrames
            facs = ["_fac%d"%i for i in range(1,1+12)]
            W = pd.DataFrame(W, columns=facs[:n_fac], index=self.df["date_full_hour"])
            H = pd.DataFrame(H, columns=self.gases, index=facs[:n_fac])
            
            # print("W", W)
            # print("H", H)
            print("n_iter =", model.n_iter_)
            
            #check quality of approximation
            """
            prod = W.dot(H)
            frac = 0.05
            counters = []
            for j in range(len(columns)):
                counter = 0
                tot = 0
                for i in range(df.shape[0]):
                    tot += 1
                    ratio = prod[i][j]/df.iloc[i,j]
                    if not 1-frac <= ratio <= 1+frac:
                        counter += 1
                        # print(i,j,"{0:.2f} {1:.2f} {2:.3f}".format(prod[i,j],df.iloc[i,j],ratio))
                # print("{0:20s}: {1:4d} / {2:4d}".format(columns[j],counter,tot))
                counters += [counter]
            print("On average, {0:.2f} / {1:4d} = {2:.0f}% of elements of columns of W*H deviate by more than {3:.1f}% from original data".format(np.mean(counters),tot,100*np.mean(counters)/tot,frac*100))
            #"""
            
            # H_sum = H.sum()
            # H_rel = H/H.sum()
            # W_sum = W.sum()
            # W_rel = W/W.sum()
            # summe_prod = np.sum(prod, axis=0)
            # summe_df = np.sum(df, axis=0)
            # quot = summe_prod/summe_df
            # for j in range(len(summe_H)):
            #     print(j, columns[j], summe_H[j], summe_prod[j], summe_df[j], quot[j])
            # print(summe_H)
            
            add = "_%s"%solver
            if solver=="mu":
                add += "_bl="+beta_loss
            label = self.gaslist + add + "_%d factors"%n_fac
            if normed:
                label += "_normed"
            
            #create an instance of class NMF to save results of performed NMF
            self.NMFs[label] = NMF(label, self.datecut, self.gaslist, df, self.properties, 
                                    W, H, n_fac, model.n_iter_, solver, beta_loss, normed)
            self.NMFs[label].save("results/NMFs/"+self.folder+label)
        
        self.hasNMF = True
    
    def plot_NMFs_Qs(self, n_factors, solver="cd", beta_loss="kullback-leibler", path=None, save=False):
        """
        Plot the Q-values of a certain series of NMFs. The desired series 
        of NMFs is defined through a list of numbers of factors and a 
        solver/beta_loss combination.
        
        For details of the Q-value calulation, see documentation of NMF.calc_Q().
        
        Can only be executed if the dict self.NMFs was previously populated 
        (by self.NMF()).

        Parameters
        ----------
        n_factors : list
            A list of numbers of factors to be compared with one another.
        solver : string, optional
            The used solver within the sklearn.decomposition.NMF method. 
            For details, see documentation of self.NMF(). 
            The default is "cd" (coordinate descent).
        beta_loss : string, optional
            The used beta_loss function. Only applies in case of 
            multiplicative update solving. For details, see documentation 
            of self.NMF().
            The default is "kullback-leibler" (only used if solver=="mu").
        """
        
        add = "_%s"%solver
        if solver=="mu":
            add += "_bl="+beta_loss
        # y = np.array([self.NMFs[self.gaslist + add + "_%d factors"%n].Q for n in n_factors])
        y = np.array([self.NMFs[self.gaslist + add + "_%d factors"%n].Q_Q_exp for n in n_factors])
        # ey = np.sqrt(np.array(y))/self.n_tot_measurements
        
        frame = plt.subplots()
        pu.plot(n_factors, y, frame=frame, kind="plot", zorder=2, lw=2, marker_option="o")
        title = "multiplicative update solver,\nbeta_loss=%s"%beta_loss if solver=="mu" else "coordinate descent solver"
        # ylabel = "Q-value"
        ylabel = "$Q/Q_{exp}$"
        pu.fig_ax_setup(frame, title=title, xlabel="number of factors $p$", 
                        ylabel=ylabel, xticks=n_factors, axis_formatting=1, 
                        ylim=(0,None))
        if save and path is not None:
            filename = "Q-value, " + str(n_factors) + "/NMF_Q-value_" + self.gaslist + add + "_%s factors"%str(n_factors)
            pu.save_figure(frame, filename=path+self.folder+filename, dpi=DPI, transparent=TRANSPARENT)
    
    def load_NMF(self, NMFlabel):
        """
        Reads-in a previously conducted and stored NMF with alias name NMFlabel 
        and stores it in the self.NMFs dictionary with NMFlabel as its 
        key and the corresponding NMF object as its value.
        
        *Important note*: In order to include correct factor names (and colors), 
        the general config file must specify/contain the desired ones at the 
        time of calling this function.

        Parameters
        ----------
        NMFlabel : string
            The unique label used to identify the desired NMF. 
            It has the form
                "<self.gaslist>_<solver>(_<beta_loss> [if solver=="mu"])_<n_factors> factors(_normed [if normed])".
            For details, see documentation of self.NMF().
        """
        
        folder = "results/NMFs/"+self.folder
        with open(folder+NMFlabel+".pkl", "rb") as NMFfileobj:
            self.NMFs[NMFlabel] = pickle.load(NMFfileobj)
        
        #update NMF.properties in case factor name or color changed since last performing the NMF
        config = pu.read_config_file(self.configfile)
        config_info = config["NMF"]
        raw = eval(config_info["NMF factor properties"])
        for i in range(1,1+12):
            self.properties.loc["_fac%d"%i,"index"] = raw["_fac%d"%i][0]
            self.properties.loc["_fac%d"%i,"name"] = raw["_fac%d"%i][1]
            self.properties.at["_fac%d"%i,"color"] = raw["_fac%d"%i][2]
        facs = ["_fac%d"%i for i in range(1, 1+self.NMFs[NMFlabel].n_factors)]
        self.NMFs[NMFlabel].properties.loc[facs] = self.properties.loc[facs]
    
    def find_extreme_conc_events(self, hilo="hi", nstd=3, path="results/extreme_conc_events/"):
        """
        Finds extreme concentration events based on a certain deviation 
        from the species' median. The functions looks for deviations of 
        a certain multiple of the standard deviation above or below the median. 
        
        The results are stored in the folder specified in path. Otherwise, 
        if these exact parameters had been used before when calling this 
        function, the previously calculated results are loaded and returned.
        
        Parameters
        ----------
        hilo : string, optional
            Switch to specify whether high ("hi") or low ("lo") 
            concentration events are to be found. 
            The default is "hi".
        nstd : float, optional
            The number of standard deviations an event has to deviate from 
            the species' median to be viewed as an extreme event. 
            The default is 3.

        Returns
        -------
        date_counter : dict
            A dictionary containing the dates of extreme concentration 
            events as keys and a tuple of 
                - the number of gases that showed an extreme concentration on that date, and
                - a list of these gases
            as the corresponding values.
        """
        
        path += self.gaslist+"/"
        filename = "%s_%s_nstd=%.1f_date_counter.pkl"%(self.gaslist, hilo, nstd)
        full_filename = path + "date_counter/" + filename
        
        if os.path.exists(full_filename):
            print("retrieving extreme concentration events (hilo={0:s}, nstd={1:.1f}) from previous search...".format(hilo,nstd))
            
            with open(full_filename, "rb") as fobj:
                date_counter = pickle.load(fobj)
            return date_counter
        
        else:
            print("finding extreme concentration events (hilo={0:s}, nstd={1:.1f})...".format(hilo,nstd))
            
            if not os.path.exists(path):
                os.makedirs(path+"date_counter/")
            
            f_by_gas = open(path+"%s_%s_nstd=%.1f_by_gas.txt"%(self.gaslist, hilo, nstd), "w")
            f_by_date = open(path+"%s_%s_nstd=%.1f_by_date.txt"%(self.gaslist, hilo, nstd), "w")
            
            date_counter = dict()
            for gas in self.gases:
                median = np.nanmedian(self.df[gas])
                std = np.nanstd(self.df[gas], ddof=1)
                # print("\t{0:20s} (median={1:7.3f}, std={2:7.3f})".format(gas, median, std))
                print(gas+", ", end="\r", flush=True)
                f_by_gas.write("{0:s}\n(median={1:7.3f}, std={2:7.3f})\n".format(gas, median, std))
                for index,row in self.df.iterrows():
                    if (hilo=="hi" and row.loc[gas] > median + nstd*std) or (hilo=="lo" and row.loc[gas] < median - nstd*std):
                        nstd_actual = abs(row.loc[gas]-median)/std
                        date_hh = (row["startdate"]+(row["enddate"]-row["startdate"])/2).round("H")
                        f_by_gas.write("\t- {0:s}, {1:7.3f} ppt, {2:4.1f} sigma away from median\n".format(str(date_hh),row.loc[gas],nstd_actual))
                        try:
                            date_counter[date_hh][0] += 1
                            date_counter[date_hh][1] += [gas]
                        except KeyError: #first time this date occurred
                            date_counter[date_hh]  = [1,[gas]]
                f_by_gas.write("\n")
            f_by_gas.close()
            
            date_counter = dict(sorted(date_counter.items()))
            dates_str = "\n".join("{0:s}: {1:d} {2:s}".format(str(k), v[0], str(v[1])) for k, v in date_counter.items())
            f_by_date.write(dates_str)
            f_by_date.close()
            
            #save date_counter into pkl file
            with open(full_filename, "wb") as output:
                print("  saving", filename)
                pickle.dump(date_counter, output, pickle.HIGHEST_PROTOCOL)
            
            return date_counter
    
    def gather_extreme_conc_events(self, hilo="hi", nstds=[2,3,5], nstd_must_be_in=3, n_min=1, details=False):
        """
        Gathers a set of extreme concentration events by evaluating 
        self.find_extreme_conc_events() several times for each nstd 
        contained in nstds. In order to appear in the set of selected 
        dates, the events must "be extreme by at least nstd_must_be_in 
        standard deviations". In addition, a requirement to the minimum 
        number of gas species showing such an extreme concentration can 
        be set via the n_min parameter.
        
        Example configuration:
        data.gather_extreme_conc_events(hilo="hi", nstds=[3,5], nstd_must_be_in=5, n_min=2)
        ("Find those dates on which at least 2 gases show a high concentration excess 
        by at least 5 standard deviations, and also show which gases show a high 
        concentration excess by at least 3 standard deviations on those dates.")

        Parameters
        ----------
        hilo : string, optional
            Switch to specify whether high ("hi") or low ("lo") 
            concentration events are to be gathered. 
            The default is "hi".
        nstds : list, optional
            A list of the number of standard deviations an event has to 
            deviate from the species' median to be viewed as an extreme event. 
            All listed deviations will be shown in the output file that 
            contains the gathered extreme concentration events. The higher 
            this number, the stricter the search for extreme events is. 
            The default is [2,3,5].
        nstd_must_be_in : float, optional
            The number of standard deviations an event has to 
            deviate from the species' median that needs to be fulfilled in 
            order to appear in the set of selected dates. Must be an 
            element of nstds. 
            The default is 3.
        n_min : int, optional
            The minimum number of gas species showing an extreme 
            concentration following the "nstd=nstd_must_be_in requirement". 
            The higher this number, the stricter the search for extreme events is. 
            The default is 1.
        details : bool, optional
            Boolean whether or not to show meteorological and other details 
            about the event date in the output file. 
            The default is False.
        """
        
        if not self.datecut:
            date_counters = dict()
            for nstd in nstds:
                date_counters[nstd] = self.find_extreme_conc_events(hilo, nstd)
            
            if details:
                details_dict  = dict()
                # cols = METEO_COLUMNS[:int(len(METEO_COLUMNS)/2)+1] + OWT_COLUMNS[1::2] + ["day of the week", "workday/weekend", "wind direction", "prevailing wind direction", "precise wind direction"]
                cols = METEO_COLUMNS[:int(len(METEO_COLUMNS)/2)+1] + OWT_COLUMNS[1::2] + list(OWT_LABELS.keys())[2:7] + ["day of the week", "workday/weekend", "wind direction", "prevailing wind direction", "precise wind direction"]
            
            #create dict that contains extreme events if found with nstd_must_be_in and add entries found for other nstds
            new_date_counter = dict()
            nstds_rem = nstds[:]
            nstds_rem.remove(nstd_must_be_in)
            
            for date,val in date_counters[nstd_must_be_in].items():
                if val[0] >= n_min: #require minimum number of compounds to show excess on a given date
                    if details:
                        startdate = pu.nearest(self.df["startdate"], date)
                        index = self.df.index[self.df["startdate"]==startdate]
                        series = self.df.loc[index,cols]
                        # s = [("ind",ind,"val",val.iloc[-1]) for ind,val in series.items()]#", ".join("{0:s}={1:s}".format(ind,val) for ind,val in series.iteritems())
                        s = ", ".join("{0:26s}={1:8s}".format(str(ind),str(val.iloc[-1])) for ind,val in series.iteritems())
                        s = pu.nth_repl_all(s, ", ", ", \n", 5)
                        details_dict[date] = s
                        # print(startdate,index,details_dict[date])
                    new_date_counter[date] = {"nstd=%.1f"%nstd_must_be_in : val}
                    for nstd in nstds_rem:
                        if date in date_counters[nstd]:
                            new_date_counter[date]["nstd=%.1f"%nstd] = date_counters[nstd][date]
            
            for k,v in new_date_counter.items():
                new_date_counter[k] = dict(sorted(v.items()))
            
            filename = "results/extreme_conc_events/"+"%s_%s_nstd=%s_nstd_must_be_in=%.1f"%(self.gaslist,hilo,str(nstds),nstd_must_be_in)
            if n_min>1:
                filename += "_n_min=%d"%n_min
            if details:
                dates_str = "\n\n--------------------\n".join("{0:1}:\n{1:s}\n{2:21s}- {3:s}".format(str(date), metdetail, "", str(value).replace("], ","]\n%21s- "%"").replace("{","").replace("}","")) for date, value, metdetail in zip(new_date_counter.keys(), new_date_counter.values(), details_dict.values()))
                filename += "_details"
            else:
                dates_str = "\n".join("{0:1}:\n{1:21s}{2:s}".format(str(date), "", str(value).replace("], ","]\n%21s"%"").replace("{","").replace("}","")) for date, value in new_date_counter.items())
            f_by_date = open(filename+"_by_date.txt", "w")
            f_by_date.write(dates_str)
            f_by_date.close()
            print("saving", filename+"_by_date.txt")
        else:
            print("gather_extreme_conc_events(): date period was cut before, so rather did not calculate anything. Please remove the date cut before calling gather_extreme_conc_events() to ensure full temporal coverage.")
    
    def frequency(self, column, path=None, save=False):
        """
        Plot the (absolute) frequency of the values in a certain colum of self.df 
        (mostly meant for the gas and meteorological columns) in a histogram.

        Parameters
        ----------
        column : string
            The column the histogram should be plotted for.
        """
        
        #exclude NaNs in the desired column
        df = self.df.loc[~self.df[column].isna()]
        df = df[column]
        
        bins = 20
        
        fig,ax = plt.subplots()
        df.hist(ax=ax, bins=bins, color="navy")
        
        print(df.value_counts())
        
        title = "frequency distribution %s"%self.properties.loc[column,"name"]
        xlabel = self.properties.loc[column,"name"] + " [%s]"%self.properties.loc[column,"unit"]
        ylabel = "frequency"
        
        pu.fig_ax_setup(fig, title=title, xlabel=xlabel, ylabel=ylabel)
        
        if save and path is not None:
            filename = ""
            if column in self.gases:
                path += "gases/"
                filename += "%d-"%self.properties.loc[column,"index"]
            elif column in METEO_COLUMNS:
                path+= "meteo/"
            filename += column
            pu.save_figure(fig, filename=path+self.folder+filename, dpi=DPI)
            plt.close("all")
    
    def frequency_winddirection(self, exclude_calm_winds=None, path=None, save=False):
        """
        Plot the (absolute) frequency of the individual wind directions of the 
        meteorological data in a wind rose.

        Parameters
        ----------
        exclude_calm_winds : float, optional
            Measurements at wind speeds of exclude_calm_winds m/s or lower 
            are excluded. The higher this value, the more significant the 
            direction may be, but one also loses statistics. 
            The unit is meters/second (m/s). The default is 1.0.
        """
        
        # import windrose as wr
        import windrose_custom as wr # Despite a couple of lines (regarding the desired wind direction sectors), this module is equivalent to the windrose package. Full credit to the developers of the windrose package.
        
        by = "precise wind direction"
        nsector = 16
        angles = np.arange(0, 2*np.pi + 2*np.pi/nsector, 2*np.pi/nsector)
        angles = angles[:-1]
        
        #exclude NaNs in winddirection
        df = self.df.loc[~self.df[by].isna()]
        
        #exclude calm winds if wanted
        if exclude_calm_winds is not None:
            df = df[df["windspeed"] > exclude_calm_winds]
        df = df[by]
        
        #no. of observations in each wind sector
        n = df.groupby(df).size().reindex(THETA_LABELS, fill_value=0)
        N = len(df)
        
        #draw windrose
        fig = plt.figure(figsize=(8,8))
        ax = wr.WindroseAxes.from_ax(fig=fig, theta_labels=THETA_LABELS)
        patch = ax.fill(angles, n, facecolor=(0,0,0,0.2), edgecolor="black", lw=2.5, zorder=2)
        
        #draw ring at expected n (if wind directional distribution was uniform)
        theta = np.arange(1000)*2*np.pi/1000
        ax.plot(theta, (N/16)*np.ones(1000), "crimson", alpha=0.7, zorder=1)
        
        #add number of observations to plot
        for index,angle,num in zip(n.index,angles,n):
            thetapos = angle if index!="E" else -0.04
            ax.annotate(num, xy=(thetapos, 0.96*ax.get_ylim()[1]),  #theta, radius
                        color="crimson", weight=650, ha="center", va="center", 
                        bbox=dict(boxstyle="round", facecolor="wheat", alpha=1.0, pad=0.1))
        
        title = "frequency distribution wind direction, \nexclude wind speeds < {0:.1f} m/s".format(exclude_calm_winds) if exclude_calm_winds is not None else "frequency distribution wind direction, \nany wind speed"
        title = None
        plt.xticks(weight="semibold")
        pu.fig_ax_setup(fig, title=title)
        
        #print some info
        # print("n",n)
        # print("bins",ax._info["bins"])
        # print("table",ax._info["table"])
        
        if save and path is not None:
            filename = "winddirection"
            if exclude_calm_winds is not None:
                filename += "_windspeed_greater_than_{0:.1f}".format(exclude_calm_winds)
            pu.save_figure(fig, filename=path+"meteo/"+self.folder+filename+".png", dpi=DPI)
    
    def frequency_OWT(self, by, path=None, save=False):
        """
        Plot the (absolute) frequency of the occurring OWTs in a histogram.

        Parameters
        ----------
        by : string
            The column the histogram should be plotted for. Can be any of the 
            OWT columns.
        """
        
        # print(by)
        df = self.df[by]
        n = df.groupby(df).size() #to also include NaNs here (so that they show up in the plots [without any boxplot])
        
        index = OWT_LABELS[by]
        n = n.reindex(index, fill_value=0)
        # print(n)
        
        figwidth = 15 if len(n)>8 else 5+len(n)
        xtick_rotation = 90 if "@" in by else 0
        width = 0.8 if "@" in by else 0.5
        
        n = n.sort_values(ascending=False)
        
        fig,ax = plt.subplots(figsize=(figwidth,7))
        n.plot.bar(color="navy", width=width)
        
        title = "frequency distribution %s"%self.properties.loc[column,"name"]
        xlabel = by #"objective weather type (OWT)"
        if by=="OWT@t+0h (forecast) label":
            xlabel = OWTlabel
        ylabel = "frequency"
        
        pu.fig_ax_setup(fig, title=title, xlabel=xlabel, ylabel=ylabel, 
                        xtick_rotation=xtick_rotation, legend_position=None)
        
        if save and path is not None:
            path+= "meteo/"
            pu.save_figure(fig, filename=path+self.folder+by, dpi=DPI)
            plt.close("all")
    
    def meteo_data_analysis(self):
        """
        This is a rather preliminary/highly customized function to analyse, for 
        example, differences in the day and night temperatures of the 
        meteorological data.
        
        Needs adjustments to be applied in a different environment.
        """
        
        def get_color(series):
            return
        
        meteo_fields = ["time", "WINDGESCHWINDIGKEIT_MITTEL", "WINDRICHTUNG_MITTEL_GRAD", "REL_FEUCHTE_PROZ", "LUFTTEMPERATUR_C", "TAUPUNKTTEMPERATUR_C", "LUFTDRUCK_STATIONSHOEHE_HPA"]
        OWT_fields = ["datetime"] + OWT_COLUMNS
        
        #read in meteo data
        meteo_df = pd.read_csv(self.meteofile, delimiter=";", usecols=meteo_fields)
        meteo_df = meteo_df.rename(columns=METEO_RENAME_DICT)
        
        meteo_df = meteo_df.loc[(meteo_df["time"]>41640) & (meteo_df["time"]<42735)]
        print(len(meteo_df["time"].apply(np.floor).value_counts()))
        
        meteo_df["is_day"] = (meteo_df["time"]-meteo_df["time"].apply(np.floor) > 12./24) & (meteo_df["time"]-meteo_df["time"].apply(np.floor) < 12.1/24)
        # meteo_df["is_day"] = (meteo_df["time"]-meteo_df["time"].apply(np.floor) > 8./24) & (meteo_df["time"]-meteo_df["time"].apply(np.floor) < 20./24)
        meteo_df["is_night"] = (meteo_df["time"]-meteo_df["time"].apply(np.floor) > 0./24) & (meteo_df["time"]-meteo_df["time"].apply(np.floor) < 0.1/24)
        # meteo_df["is_night"] = (meteo_df["time"]-meteo_df["time"].apply(np.floor) > 0.5/24) & (meteo_df["time"]-meteo_df["time"].apply(np.floor) < 1.15/24)
        # meteo_df["is_night"] = ~meteo_df["is_day"]
        meteo_df.loc[meteo_df["is_day"], "color"] = "orange"
        meteo_df.loc[~meteo_df["is_day"], "color"] = "navy"
        
        # meteo_df.plot.scatter(x="time", y="air temperature", color=meteo_df["color"])
        # print(meteo_df)
        T = meteo_df["air temperature"].mean()
        T_std = meteo_df["air temperature"].std()
        T_err = T_std/np.sqrt(len(meteo_df["air temperature"]))
        day_T = meteo_df.loc[meteo_df["is_day"], "air temperature"].mean()
        night_T = meteo_df.loc[meteo_df["is_night"], "air temperature"].mean()
        day_T_std = meteo_df.loc[meteo_df["is_day"], "air temperature"].std()
        night_T_std = meteo_df.loc[meteo_df["is_night"], "air temperature"].std()
        day_T_err = day_T_std/np.sqrt(meteo_df["is_day"].value_counts()[True])
        night_T_err = night_T_std/np.sqrt(meteo_df["is_night"].value_counts()[True])
        
        # print(meteo_df)
        print("all:   %.3f +- %.3f"%(T,T_err))
        print("day:   %.3f +- %.3f"%(day_T,day_T_err))
        print("night: %.3f +- %.3f"%(night_T,night_T_err))


class NMF:
    """
    This is a class that stores results of a non-negative matrix factorization 
    (NMF). It contains information about the included gases, their 
    uncertainties, the initial concentration data (self.X), the two matrices 
    that X was decomposed into (X ~ W*H), as well as metadata such as the 
    number of factors or iterations that were needed to find that solution.
    
    Using this class, one can
        - plot each gas' contribution to each of the factors of the NMF (plot_factor_contributions()),
        - plot time series of the factors (plot_factor_timeseries(), plot_one_factor_timeseries()), and
        - calculate Q-values (see documentation of calc_Q()).
    
    Non-negative matrix factorization. The data (df) is decomposed (factorized) 
    into a matrix product W*H that is supposed to approximate the given data 
    as accurately as possible. 
    The resulting dimensions are:
        dim(df) = ( self.n_tot_measurements, len(self.gases) )
        dim(W)  = ( self.n_tot_measurements, n_factors )
        dim(H)  = ( n_factors, len(self.gases) )
    The corresponding label (self.label) is of the form
        "<gaslist>_<solver>(_<beta_loss> [if solver=="mu"])_<n_factors> factors(_normed [if normed])"
    """
    
    def __init__(self, label, datecut, gaslist, X, properties, W, H, n_factors, n_iter, solver, beta_loss=None, normed=False):
        self.label = label #alias name such as "all gases_cd_5 factors"
        self.datecut = datecut
        self.folder = "full time range/" if not self.datecut else self.datecut+"/"
        self.gaslist = gaslist
        self.gases = list(X.columns)
        self.X = X
        self.n = self.X.shape[0]
        self.m = self.X.shape[1]
        self.X.index = W.index
        facs = ["_fac%d"%i for i in range(1,1+n_factors)]
        self.unc = properties.loc[self.gases,"uncertainty"]
        self.properties = properties.loc[facs+self.gases] #needed for the factors' properties like color etc.
        self.W = W
        self.H = H
        self.prod = W.dot(H)
        self.E = self.X - self.prod
        self.W_rel = W/W.sum()
        self.H_rel = H/H.sum()
        
        self.n_factors = n_factors
        self.n_iter = n_iter
        self.solver = solver
        self.beta_loss = None if self.solver == "cd" else beta_loss
        
        self.normed = normed #whether or not the data was normed to each species' median before performing the NMF
        
        self.calc_Q()
        print("Q     = {:,.0f}".format(round(self.Q, -1)))
        print("Q_exp = {:,.0f}".format(round(self.Q_exp, -1)))
    
    def __str__(self):
        string = "Non-negative matrix factorization, performed with the sklearn.decomposition package\
                \n\talias name = {0:s}\
                \n\tdatecut    = {1:s}\
                \n\tgaslist    = {2:s}\
                \n\tdim(X)     = {3:d} x {4:d}\
                \n\tn_factors  = {5:d}\
                \n\tsolver     = {6:s}".format(self.label, self.datecut, self.gaslist, self.n, self.m, self.n_factors, self.solver)
        if self.solver != "cd":
            string += "\n\tbeta_loss  = {0:s}".format(self.beta_loss)
        return string+"\n"
    
    def save(self, filename):
        if not filename.endswith(".pkl"):
            filename += ".pkl"
        
        #create path if it does not exist yet
        path = "/".join(filename.split("/")[:-1])
        if not os.path.exists(path):
            os.makedirs(path)
        
        with open(filename, "wb") as output:
            print("saving", filename)
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)
    
    def pick_date_interval(self, start=None, end=None):
        """
        Sets the startdate and enddate of the NMF after having calculated it. 
        That is, the dataframes X, W, prod, E, and W_rel are being cut to 
        the given date range.
        
        The variable self.datecut is set to a string representing the 
        updated startdate and enddate.

        Parameters
        ----------
        start : string, optional
            Date of the new startdate. The default is None, which leaves the startdate unchanged.
        end : string, optional
            Date of the new enddate. The default is None, which leaves the enddate unchanged.
        """
        
        if start is not None:
            self.startdate = pd.to_datetime(start)
        if end is not None:
            self.enddate   = pd.to_datetime(end)
        
        if start is not None or end is not None:
            self.datecut = str(self.startdate.date())+"_"+str(self.enddate.date())
            indices = self.X.index[(self.startdate < self.X.index.date) & (self.X.index.date < self.enddate)]
            self.X = self.X.loc[indices]
            self.W = self.W.loc[indices]
            self.prod = self.prod.loc[indices]
            self.E = self.E.loc[indices]
            self.W_rel = self.W_rel.loc[indices]
        
        self.folder = "full time range/" if not self.datecut else self.datecut+"/"
    
    def calc_Q(self):
        """
        Calculates the (absolute) Q-value of the NMF.
        
        The Q-value is a measure for the deviation of the matrix product W*H 
        (which is supposed to approximate X) from the input data X. It sums 
        the quadratic deviation for all matrix elements (i.e. measurements) 
        weighted by the respective gas' uncertainty.
        """
        
        Q = 0
        for gas in self.gases:
            sigma = self.unc[gas] if "new gases" not in self.gaslist else np.nanstd(self.X[gas], ddof=1) #self.unc is currently the data.properties["uncertainty"] variable
            for index,row in self.X.iterrows():
                Q += self.E.loc[index,gas]**2/sigma**2
        self.Q = Q
        
        #calculate expected Q-value, see literature
        self.Q_exp = (self.n-self.n_factors) * (self.m-self.n_factors)
        self.Q_Q_exp = self.Q/self.Q_exp
    
    def plot_factor_contributions(self, plot="absrel", path=None, save=False):
        """
        Plots the NMF's factor contributions (i.e. the matrix H) as bar plots.

        Parameters
        ----------
        plot : string, optional
            Plot either absolute ("abs"), or relative ("rel"), or absolute and 
            relative contributions next to each other ("absrel"). 
            The default is "absrel".
        """
        
        print("{0:<37s}{1:s}...".format("plotting NMF factor contributions of ",self.label))
        
        facs = ["_fac%d"%i for i in range(1,1+12)]
        if path is not None:
            path += self.gaslist + "/"
        
        if plot=="absrel": #plot abs and rel
            fig,axs = plt.subplots(self.n_factors, 2, figsize=(2*6.4*2*len(self.gases)/52,4.8*1.5*self.n_factors/6))
            # suptitle = "NMF\n%d factors"%self.n_factors
            suptitle = "NMF factor contributions\nmu: beta_loss={0:s}, {1:d} factors".format(self.beta_loss,self.n_factors) if self.solver=="mu" else "NMF factor contributions\ncd, {0:d} factors".format(self.n_factors)
            if self.normed:
                suptitle += ", normed"
            fig.suptitle(suptitle, y=0.99)
            for i,ax in enumerate(axs):
                fac = facs[i]
                color = self.properties.loc[fac,"color"]
                label = self.properties.loc[fac,"name"]
                ax[0].bar(range(self.H.shape[1]), self.H.loc[fac], align="center", alpha=0.7, color=color, label=label)
                ax[1].bar(range(self.H.shape[1]), self.H_rel.loc[fac], align="center", alpha=0.7, color=color)
                for a in ax:
                    a.set_xticks(range(self.H.shape[1]))
                    a.set_xticklabels(())
                    a.xaxis.grid(0)
                ax[1].set_ylim((0,1))
                ax[0].legend()
            xticklabels = self.properties.loc[self.gases, "short name"]
            axs[-1,0].set_xticklabels(xticklabels, rotation=90)
            axs[-1,1].set_xticklabels(xticklabels, rotation=90)
            ylabel0 = "mass of gas species apportioned to factor [ppt]"
            ylabel1 = "fraction of gas species apportioned to factor"
            pu.add_common_ytitle(ylabel0, 121)
            pu.add_common_ytitle(ylabel1, 122)
            plt.subplots_adjust(wspace=0.3, top=0.85+0.08*self.n_factors/11)
            
            if save and path is not None:
                filename = "NMF fac conts_" + self.label
                pu.save_figure(fig, filename=path+self.folder+filename, dpi=DPI, transparent=TRANSPARENT)
        
        else: #plot ONLY abs OR rel
            dic = {"rel": True, "abs": False}
            plot_rel = dic[plot]
            fig,axs = plt.subplots(self.n_factors, 1, figsize=(6.4*2*len(self.gases)/52,4.8*1.5*self.n_factors/6))
            kind = "relative" if plot_rel else "absolute"
            title = kind+" NMF factor contributions\nbeta_loss={0:s}, {1:d} factors".format(self.beta_loss,self.n_factors)
            if self.normed:
                title += ", normed"
            axs[0].set_title(title)
            for i,ax in enumerate(axs):
                fac = facs[i]
                y = self.H_rel.loc[fac] if plot_rel else self.H.loc[fac]
                ax.bar(range(self.H.shape[1]), y, align="center", alpha=0.6, color="blue")
                ax.set_xticks(range(self.H.shape[1]))
                ax.set_xticklabels(())
                ax.xaxis.grid(0)
                if plot_rel:
                    ax.set_ylim((0,1))
            axs[-1].set_xticklabels(self.gases, rotation=90)
            ylabel = "fraction of gas species apportioned to factor" if plot_rel else "mass of gas species apportioned to factor [ppt]"
            pu.add_common_ytitle(ylabel)
            plt.subplots_adjust(wspace=0.3, top=0.85+0.08*self.n_factors/11)
            
            if save and path is not None:
                filename = "NMF fac conts_" + kind + "_" + self.label
                pu.save_figure(fig, filename=path+self.folder+filename, dpi=DPI, transparent=TRANSPARENT)
    
    def plot_factor_timeseries(self, path=None, save=False):
        """
        Plots the NMF's factor time series (i.e. the matrix W), all in one 
        figure, in separate subplots.
        Creates one figure (one file) per call.
        
        Parameters
        ----------
        No specific parameters.
        """
        
        print("{0:<37s}{1:s}...".format("plotting NMF factor time series of ",self.label))
        
        frame = plt.subplots(self.n_factors, 1, sharex=True)
        fig,axs = frame
        
        for n in range(1,1+self.n_factors):
            self.plot_one_factor_timeseries(n, frame=[fig,axs[n-1]], setup=False) #plot each factor one-by-one into one common frame
        
        fig.set_size_inches(8, 2.*self.n_factors)
        suptitle = "NMF factor time series\nmu: beta_loss={0:s}, {1:d} factors".format(self.beta_loss,self.n_factors) if self.solver=="mu" else "NMF factor time series\ncd, {0:d} factors".format(self.n_factors)
        if self.normed:
            suptitle += ", normed"
        fig.suptitle(suptitle, y=0.99)
        # ylabel = ""
        ylabel = "factorial mass [ppt]"
        pu.add_common_ytitle(ylabel, 111)
        plt.xlabel(xlabel)
        
        plt.subplots_adjust(wspace=0.3, top=0.85+0.08*self.n_factors/11)
        for i,name in enumerate(self.properties["name"]):
            if i<self.n_factors:
                axs[i].set_title(name, fontsize=11, pad=2.5)
        
        if save and path is not None:
            filename = "NMF timeseries_" + self.label
            pu.save_figure(frame, filename=path+self.gaslist+"/"+self.folder+filename, dpi=DPI, transparent=TRANSPARENT)
    
    def plot_one_factor_timeseries(self, factor_ind, frame=None, relative=False, 
                                   label=False, color=None, alpha=1., setup=True):
        """
        Plots one of the NMF's factors' time series (i.e. one column of the 
        matrix W). The result is what is shown in each subplot of the figure 
        created in self.plot_factor_timeseries().
        Creates one figure (one file) per call.
        
        Parameters
        ----------
        factor_ind : int
            Index of the factor that is to be plotted. 
            Can be in the range from 1 to self.n_factors.
        frame : tuple of matplotlib figure and ax instances, optional
            The frame (== [fig,ax]) of the matplotlib figure. 
            The default is None (will take the current frame, or create a new one).
        relative : bool, optional
            Boolean whether or not to plot W_rel instead of W (W_rel = W/W.sum()).
            The default is False (plots W).
        label : bool, optional
            Boolean whether or not to add a label to the plot's legend. 
            The default is False.
        color : color type, optional
            The color that is to be used for the plotted time series. 
            The default is None, which will use the color that was specified 
            for this NMF factor in the configuration file.
        alpha : float, optional
            The transparency of the plotted line. Ranges from 0 (fully 
            transparent) to 1 (not transparent).
            The default is 1.0.
        setup : bool, optional
            Boolean whether or not to call the setup function on the figure. 
            If True, then this adds a title to the figure (the factor's name) 
            avoids the creation of a legend.
            The default is True.

        Returns
        -------
        frame : tuple of matplotlib figure and ax instances
            The frame (== [fig,ax]) of the (created) matplotlib figure.
        """
        
        #get factor's name
        facs = ["_fac%d"%i for i in range(1,1+self.n_factors)]
        fac = facs[factor_ind-1]
        
        label = self.properties.loc[fac,"name"] if label else None
        
        if frame is None:
            frame = [plt.gcf(), plt.gca()] #[plt.figure(column),plt.subplot(1,1,1)]
        frame[0].set_size_inches(10, 5)
        
        if relative:
            y = self.W_rel[fac]
        else:
            y = self.W[fac]
        
        date = pd.to_datetime("2018-03-01")
        if date in y.index.date:
            y.loc[date-pd.Timedelta(days=1)] = None
            y = y.sort_index()
        
        if color is None:
            color = self.properties.loc[fac,"color"]
        
        pu.plot(y.index, y, frame=frame, kind="plot",
                marker_option="", color=color, 
                label=label, alpha=alpha)
        
        if setup:
            title = self.properties.loc[fac,"name"]
            pu.fig_ax_setup(frame, title=title, legend_position=None)
        
        return frame


class Station:
    """
    Class containing information about a measurement site.
    
    Information is retrieved and stored from the station configuration file. 
    Its filename has to correspond to the "station = ..." entry in the 
    dataset's configuration file.
    """
    
    def __init__(self, name, **kwargs):
        self.name = name
        
        station_info = pu.read_config_file(self.name+".cfg")
        
        self.code = station_info["General Info"]["code"]
        self.WDCA_ID = station_info["General Info"]["WDCA_ID"]
        self.GAW_ID = station_info["General Info"]["GAW_ID"]
        self.land_use = station_info["General Info"]["land_use"]
        self.setting = station_info["General Info"]["setting"]
        self.latitude = float(station_info["General Info"]["latitude"])
        self.longitude = float(station_info["General Info"]["longitude"])
        self.altitude = float(station_info["General Info"]["altitude"])
    
    def __str__(self):
        return "station info: \
                \n\tname      = {0:s}\n\tcode      = {1:s}\n\tWDCA-ID   = {2:s} \
                \n\tGAW-ID    = {3:s}\n\tland use  = {4:s}\n\tsetting   = {5:s} \
                \n\tlatitude  = {6:f}\n\tlongitude = {7:f}\n\taltitude  = {8:f}".format( 
                self.name, self.code, self.WDCA_ID, self.GAW_ID, self.land_use, \
                self.setting, self.latitude, self.longitude, self.altitude)
    
    def get_pos(self):
        return (self.latitude, self.longitude, self.altitude)

