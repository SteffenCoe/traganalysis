# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 12:32:15 2020

@author: steff

This is the 'main' file to be used to analyze the given dataset.
Here, the dataset can be read-in and all functions defined in classes.py 
can be executed.
"""

###############################################################################
"""
Imports.
"""

import timeit
start = timeit.default_timer()

import matplotlib.pyplot as plt
import pandas as pd

import classes
import utils as pu
import config.init
config.init.run()

from GLOBAL import *

###############################################################################
"""
Retrieve data.
"""

#source files
configfile = "config.cfg"
datafile   = "data/UFS_FCKW_2013-2019.csv"
meteofile  = "data/meteodata/meteo_ufs-sfh_2013-2019.csv"

#instantiate dataset
data = classes.Dataset(configfile, datafile, meteofile)

#read in data
# data.initialize_data()
# data.initialize_meteodata()
# data.initialize_additional_info()
# data.read_dataset(cleaned=False)
data.read_dataset(cleaned=True)

# data.set_gaslist("all gases")
# data.set_gaslist("any seasonality, any slope, extreme peaks")
# data.set_gaslist("high seasonality, any slope, any peaks")
# data.set_gaslist("any seasonality, clearly positive slope, any peaks")
# data.set_gaslist("any seasonality, clearly negative slope, any peaks")
# data.set_gaslist("halocarbons")
data.set_gaslist("old gases")
# data.set_gaslist("old gases: CFCs")
# data.set_gaslist("old gases: HCFCs")
# data.set_gaslist("old gases: HFCs")
# data.set_gaslist("old gases: halons")
# data.set_gaslist("old gases (w/o CCl4, HFC-152a)")
# data.set_gaslist("new gases")
# data.set_gaslist("new gases: alkanes")
# data.set_gaslist("new gases: alkenes")
# data.set_gaslist("new gases: hydrocarbons")
# data.set_gaslist("CFCs")
# data.set_gaslist("selection 1")
# data.set_gaslist([CFC-11, CFC-12, CFC-114, CFC-115, CCl4, bromoform])
# data.set_gaslist(['bromomethane', 'methyl iodide', 'carbonyl sulfide'])
# data.set_gaslist(["HCFC-141b"])
# data.set_gaslist(["PFC-116"])
# data.set_gaslist(["tetrachloroethylene"])

if "HFC-152a" in data.gases:
    startd = "2018-03-01"
    endd   = "2018-09-07"
    indices = data.df.index[(pd.to_datetime(startd) < data.df["startdate"]) & (data.df["startdate"] < pd.to_datetime(endd))]
    data.df.loc[indices,"HFC-152a"] = None #clean clearly too high values for HFC-152a in 2018
    data.df.loc[indices,"flag_HFC-152a"] = 1.999
    data.df.loc[[40,153,154,158,161],"HFC-152a"] = None #clear false values for HFC-152a in November 2013
    data.df.loc[[40,153,154,158,161],"flag_HFC-152a"] = 1.999

# data.pick_date_interval(start="2013-04-13", end="2017-05-31") #until gap
# data.pick_date_interval(start="2013-10-19", end="2017-05-31") #until gap, without crappy beginning
data.pick_date_interval(start="2013-10-19", end="2018-11-28") #until after gap@(2017-06 - 2018-02), but before crappy ending@end(2018), without crappy beginning@(<2013-10)
# data.pick_date_interval(start="2013-10-19", end="2018-10-19")
# data.pick_date_interval(start="2018-08-01", end="2018-09-10") #after gap
# data.pick_date_interval(start="2015-04-01", end="2015-04-30")
# data.pick_date_interval(start="2015-11-01", end="2015-12-31")
# data.pick_date_interval(start=None, end="2017-05-30")
# data.pick_date_interval(start="2016-01-01", end="2017-01-01")
# data.pick_date_interval(start="2018-03-01", end="2019-02-22") #only new gases
# data.pick_date_interval(start="2013-10-19", end="2018-03-31")

#clean up data
data.clean_measurements()

#calc moving averages
data.calc_CMA(interval=3)
data.calc_CMA(interval=7)
data.calc_CMA(interval=30)

#save dataset
# data.save_dataset()

#import dataset directly from binary file
# import pickle
# with open("data/bin/dataset_cleaned.pkl", "rb") as fobj:
#     data = pickle.load(fobj)

###############################################################################
"""
Plotting timeseries.
"""

#syntax: (daily only without label, daily, 3-day CMA, 7-day CMA, 30-day CMA)
combinations = [
                    # (1,0,0,0,0), 
                    # (0,1,0,0,0), 
                    # (0,0,1,0,0), 
                    # (0,0,0,1,0), 
                    # (0,0,0,0,1), 
                    # (0,0,0,1,1), 
                    # (0,1,0,1,1), 
                    # (0,1,1,0,0), 
                    # (0,1,0,1,0), 
                    (0,1,0,0,1), 
               ]
        
# data.plot_all_timeseries(combinations, plot_gases, plot_meteoprops, save=save)

###############################################################################
"""
Further analyses. 
"""

# data.calc_stats(basic_stats=(1,1), slope_stats=(1,1))

# for prop in METEO_COLUMNS:
#     data.frequency(prop, path=out+"frequency/", save=save)
# for gas in data.gases:
#     data.frequency(gas, path=out+"frequency/", save=save)
# data.frequency_winddirection(exclude_calm_winds=1.0, path=out+"frequency/", save=save)

# for gas in data.gases:
#     data.fft(gas)

# data.boxplots_conc(path=out+"boxplots/", save=save)
# data.scatter_matrix(path=out+"correlations/")

# bys = ["OWT@t+0h (forecast) label", "OWT@t-12h label", "OWT prevailing wind direction", "OWT cyclonality", "OWT cyclonality@950hPa", "OWT cyclonality@500hPa", "OWT humidity", "day of the week", "workday/weekend", "wind direction", "prevailing wind direction", "precise wind direction"]
# bys = ["OWT@t+0h (forecast) label"]
# bys = ["day of the week", "workday/weekend"]

# for by in bys:
#     data.boxplots(by, verbose=1, plot_boxplots=1, sort=1, path=out+"boxplots/", save=save)
    # data.frequency_OWT(by, path=out+"frequency/", save=save)

# data.calc_deviation_counts("OWT@t+0h (forecast) label", mode="half")
# data.show_deviation_counts("OWT@t+0h (forecast) label", mode="half", path=out+"correlations/", save=save)
# data.calc_deviation_counts("OWT@t+0h (forecast) label", mode="quartile")
# data.show_deviation_counts("OWT@t+0h (forecast) label", mode="quartile", path=out+"correlations/", save=save)

# data.correlation_matrix(path=out+"correlations/correlation matrices/", save=save)
# data.correlation_matrix(columnsy=METEO_COLUMNS[1:7], path=out+"correlations/correlation matrices/", save=save)
# data.correlation_matrix(columnsy=METEO_COLUMNS[7:], path=out+"correlations/correlation matrices/", save=save) #cannot be executed for new gases only (since they don't have meteodata stds...)
# data.correlation_matrix(columnsx=METEO_COLUMNS[1:7], columnsy=METEO_COLUMNS[1:7], path=out+"correlations/correlation matrices/", save=save)

# data.windroses(criterion_value=0.75, exclude_calm_winds=1.0, path=out+"windroses/", save=save)
# data.windroses_CPF(criterion_value=0.75, exclude_calm_winds=1.0, exclude_low_occurances=5, path=out+"windroses/", save=save)

# data.PCA(5, path=out+"correlations/PCA/", save=save)

# data.find_extreme_conc_events(hilo="hi", nstd=2)
# data.gather_extreme_conc_events(hilo="hi", nstds=[2,3,5], nstd_must_be_in=2, n_min=4, details=False)
# data.gather_extreme_conc_events(hilo="lo", nstds=[0.5], nstd_must_be_in=0.5, n_min=12, details=False)

# rnd_dates = pu.select_random_elements_from_list(data.df["date_full_hour"], 30)
# print(rnd_dates)

beta_losses = ["frobenius", "kullback-leibler", "itakura-saito"]
beta_losses = []
n_factors = range(4,12)
# n_factors = [8]
norm_NMF = False #False is default/standard
normed = "_normed" if norm_NMF else ""
# """
#perform NMFs
for n in n_factors:
    for bl in beta_losses:
        data.NMF(n_factors=n, solver="mu", beta_loss=bl, normed=norm_NMF)
    # data.NMF(n_factors=n, solver="cd", normed=norm_NMF)
#"""

# """
#read in previously performed NMFs
# n_factors = [7,8]
for n in n_factors:
    for bl in beta_losses:
        NMFlabel = data.gaslist + "_mu_bl=%s_%d factors"%(bl,n) + normed
        data.load_NMF(NMFlabel)
    NMFlabel = data.gaslist + "_cd_%d factors"%n + normed
    data.load_NMF(NMFlabel)
#"""

# """
#plot/work with (all) previously performed NMFs
for n in n_factors:
    for bl in beta_losses:
        NMFlabel = data.gaslist + "_mu_bl=%s_%d factors"%(bl,n) + normed
        NMF = data.NMFs[NMFlabel]
        NMF.plot_factor_contributions(plot="absrel", path=out+"correlations/NMF/", save=save)
        NMF.plot_factor_timeseries(path=out+"correlations/NMF/", save=save)
    NMFlabel = data.gaslist + "_cd_%d factors"%n + normed
    NMF = data.NMFs[NMFlabel]
    NMF.plot_factor_contributions(plot="absrel", path=out+"correlations/NMF/", save=save)
    NMF.plot_factor_timeseries(path=out+"correlations/NMF/", save=save)
    plt.close("all")
#"""

# for bl in beta_losses:
#     data.plot_NMFs_Qs(n_factors=n_factors, solver="mu", beta_loss=bl, path=out+"correlations/NMF/%s/"%data.gaslist, save=save)
# data.plot_NMFs_Qs(n_factors=n_factors, solver="cd", path=out+"correlations/NMF/%s/"%data.gaslist, save=save)

# """
#plot one specific NMF's time series
n = 8
NMFlabel = "old gases_cd_%d factors"%n + normed
NMF = data.NMFs[NMFlabel]
frame_all = plt.subplots(num="NMF factor timeseries all gases")
pu.purge(out+"correlations/NMF/%s/%s/%s/"%(data.gaslist,data.folder,NMFlabel), "NMF timeseries_factor ", safe_mode=False)
for i in range(1,1+n):
    NMF.plot_one_factor_timeseries(i, frame=frame_all, setup=False, label=True, alpha=0.5)
    
    frame = plt.subplots()
    NMF.plot_one_factor_timeseries(i, frame=frame)
    pu.fig_ax_setup(frame, xlabel=xlabel, ylabel="factorial mass [ppt]", legend_position=None, 
                    filename=out+"correlations/NMF/%s/%s/%s/"%(data.gaslist,data.folder,NMFlabel)+"NMF timeseries_factor %d (%s)"%(i,NMF.properties["name"].iloc[i-1]))

pu.fig_ax_setup(frame_all, xlabel=xlabel, ylabel="factorial mass [ppt]", 
                legend_position="center left", bbox_to_anchor=(1, 0.5), ncol=1, 
                filename=out+"correlations/NMF/%s/%s/%s/"%(data.gaslist,data.folder,NMFlabel)+"NMF timeseries_all")
#"""

###############################################################################

#save dataset
# data.save_dataset()

#print runtime
stop = timeit.default_timer()
print("\nruntime = {0:.2f} s".format(stop-start))
