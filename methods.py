# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 17:26:08 2020

@author: steff

Useful and necessary helper functions used by some functions in classes.py.
"""

###############################################################################
""" Imports. """

import numpy as np
import pandas as pd

import utils as pu

###############################################################################
""" Methods used in the analysis. """

def get_OWTs():
    return eval(pu.read_config_file("OWTC.cfg")["General"]["OWTs"])

def get_all_OWT_labels():
    return np.array(list(get_OWTs().values()))[:,0]

def OWT_number_to_label(OWT_number):
    OWTs = get_OWTs()
    return OWTs[OWT_number][0]

def OWT_label_to_number(OWT_label):
    OWTs = get_OWTs()
    for number,label in OWTs.items():
        if label == OWT_label:
            return number

def get_OWL_to_OWT():
    return eval(pu.read_config_file("OWTC.cfg")["General"]["OWL_to_OWT"])

def get_wind_dir(deg, mode="normal"):
    sep = 22.5
    width = 2*sep
    if mode=="normal":
        if 7*width+sep <= deg < 360 or 0 <= deg < 0*width+sep:
            return "N"
        elif 0*width+sep <= deg < 1*width+sep:
            return "NE"
        elif 1*width+sep <= deg < 2*width+sep:
            return "E"
        elif 2*width+sep <= deg < 3*width+sep:
            return "SE"
        elif 3*width+sep <= deg < 4*width+sep:
            return "S"
        elif 4*width+sep <= deg < 5*width+sep:
            return "SW"
        elif 5*width+sep <= deg < 6*width+sep:
            return "W"
        elif 6*width+sep <= deg < 7*width+sep:
            return "NW"
    if mode=="prevailing":
        if 0 <= deg < 90:
            return "NE"
        elif 90 <= deg < 180:
            return "SE"
        elif 180 <= deg < 270:
            return "SW"
        elif 270 <= deg < 360:
            return "NW"
    if mode=="precise":
        sep /= 2
        width /= 2
        if 15*width+sep <= deg < 360 or 0 <= deg < 0*width+sep:
            return "N"
        elif 0*width+sep <= deg < 1*width+sep:
            return "NNE"
        elif 1*width+sep <= deg < 2*width+sep:
            return "NE"
        elif 2*width+sep <= deg < 3*width+sep:
            return "ENE"
        elif 3*width+sep <= deg < 4*width+sep:
            return "E"
        elif 4*width+sep <= deg < 5*width+sep:
            return "ESE"
        elif 5*width+sep <= deg < 6*width+sep:
            return "SE"
        elif 6*width+sep <= deg < 7*width+sep:
            return "SSE"
        elif 7*width+sep <= deg < 8*width+sep:
            return "S"
        elif 8*width+sep <= deg < 9*width+sep:
            return "SSW"
        elif 9*width+sep <= deg < 10*width+sep:
            return "SW"
        elif 10*width+sep <= deg < 11*width+sep:
            return "WSW"
        elif 11*width+sep <= deg < 12*width+sep:
            return "W"
        elif 12*width+sep <= deg < 13*width+sep:
            return "WNW"
        elif 13*width+sep <= deg < 14*width+sep:
            return "NW"
        elif 14*width+sep <= deg < 15*width+sep:
            return "NNW"

def slope_stats(df, ey):
    """
    Performs a linear regression on df with x = df.iloc[:,0], y = df.iloc[:,1], 
    and ey = ey.

    Parameters
    ----------
    df : pandas.DataFrame
        DataFrame to perform the linear regression on. Supposed to consist of 
        two columns, where the first will be taken as x and the second as y 
        in the linear regression.
    ey : float
        Uncertainty on all y-values.

    Returns
    -------
    out : pandas.Series
        A pandas Series object that contains the results of the linear regression.
    """
    
    #x = df.iloc[:,0]
    x = (df.iloc[:,0] - df.iloc[0,0]).values
    y = df.iloc[:,1]
    out = pd.Series(name=df.iloc[:,1].name)
    results = pu.lineare_regression(x, y, [ey])
    slope,eslope,b,eb,chiq,cov = results
    chiqndof = chiq/(len(x)-2)
    nsigma_slope = pu.n_sigma(slope,eslope)
    out.loc["slope"] = slope*365 #to get unit ppt/year
    out.loc["eslope"] = eslope*365 #to get unit ppt/year
    out.loc["nsigma_slope"] = nsigma_slope
    out.loc["b"] = b
    out.loc["eb"] = eb
    out.loc["chiq"] = chiq
    out.loc["chiqndof"] = chiqndof
    out.loc["cov"] = cov
    return out
