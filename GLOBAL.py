# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 10:42:15 2020

@author: steff

This file contains the global variables used throughout the analysis.
Here, dictionaries helping to read-in the data as well as switches and 
parameters for the plotting and saving are defined.
"""

import matplotlib.pyplot as plt

import methods

###############################################################################
"""
Set global variables.
"""

#regarding saving figures
DPI = 200 #None #100
out = "plots/"
save = 0
TRANSPARENT = 0

plot_gases      = 1
plot_meteoprops = 0

plt.rcParams.update({'figure.max_open_warning': 0})
xlabel = "time"
ylabel = "mixing ratio [ppt]"

colors = { 
           "daily": "navy",
           "3-day": "yellowgreen",
           "7-day": "orange",
           "30-day": "red",
    }

OWTlabel = "OWT at midnight (forecast)"

###############################################################################
"""
Needed in classes.py.
"""

METEO_RENAME_DICT = { "WINDGESCHWINDIGKEIT_MITTEL": "windspeed", 
                      "WINDRICHTUNG_MITTEL_GRAD": "winddirection", 
                      "REL_FEUCHTE_PROZ": "relative humidity", 
                      "LUFTTEMPERATUR_C": "air temperature", 
                      "TAUPUNKTTEMPERATUR_C": "dewpoint temperature", 
                      "LUFTDRUCK_STATIONSHOEHE_HPA": "air pressure"
    }
METEO_COLUMNS = ["num_used_meteodata"] + list(METEO_RENAME_DICT.values()) + [prop+" std" for prop in METEO_RENAME_DICT.values()]
OWT_COLUMNS = []
for i in ["OWT@t-12h", "OWT@t+0h (forecast)"]:
# for i in ["OWT@t-12h", "OWT@t+0h (forecast)", "OWT@t+12h (forecast)", "OWT@t+24h (forecast)", "OWT@t+36h (forecast)", "OWT@t+48h (forecast)", "OWT@t+60h (forecast)", "OWT@t+84h (forecast)", "OWT@t+108h (forecast)", "OWT@t+132h (forecast)", "OWT@t+156h (forecast)"]:
    OWT_COLUMNS += [i+" number",i+" label"]
OWT_LABELS = { "OWT@t-12h label": methods.get_all_OWT_labels(),
               "OWT@t+0h (forecast) label": methods.get_all_OWT_labels(),
               "OWT prevailing wind direction": ["NW","SW","SE","NE","XX"],
               "OWT cyclonality": ["AA", "AC", "CA", "CC"],
               "OWT cyclonality@950hPa": ["A", "C"],
               "OWT cyclonality@500hPa": ["A", "C"],
               "OWT humidity": ["D", "W"]
    }
WORKDAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
WEEKEND = ["Saturday", "Sunday"]
LABELS = { "day of the week": WORKDAYS+WEEKEND,
           "workday/weekend": ["workday","weekend"],
           "wind direction": ["N","NE","E","SE","S","SW","W","NW"],
           "prevailing wind direction": ["NE","SE","SW","NW"],
           "precise wind direction": ["N","NNE","NE","ENE","E","ESE","SE","SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"]
    }
THETA_LABELS = ["E","ENE","NE","NNE","N","NNW","NW","WNW","W","WSW","SW","SSW","S","SSE","SE","ESE"]

#the desired blank number of days to the left and right in each time series plot
SEC = 103 #103 == number of days in 2013 until April 14th (fitted well in my study)
